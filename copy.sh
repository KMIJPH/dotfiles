find . ! -name '.' ! -path "*.git*" -type d -exec rm -rf {} +
find . ! -name 'copy.sh' ! -name 'link.sh' ! -name 'dotool.json' ! -path "*.git*" -type f -exec rm -f {} +
dotool -c -s dotool.json

