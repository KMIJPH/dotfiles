#!/usr/bin/env lua
-- File Name: mail.lua
-- Description: Mail settings (aerc)
-- Author: KMIJPH
-- Repository: __masked__
-- License: GPLv3+
-- Creation Date: 09 Mar 2023 22:49:48
-- Last Modified: 02 Jul 2023 10:27:39

vim.api.nvim_cmd({ cmd = "setlocal", args = { "spell" } }, {})
vim.api.nvim_cmd({ cmd = "setlocal", args = { "textwidth=0" } }, {})
