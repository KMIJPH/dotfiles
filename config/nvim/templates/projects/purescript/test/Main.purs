-- File Name: Main.purs
-- Description:
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date:
-- Last Modified:

module Test.Main where

import Prelude

import Test.Unit (test)
import Effect (Effect)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)

main :: Effect Unit
main = runTest do
  test "main" do
     equal 1 1
