// File Name: main_test.go
// Description:
// Author: KMIJPH
// Repository: codeberg.org/KMIJPH
// License: GPL3
// Creation Date:
// Last Modified:

package main

import (
	"testing"
)

func TestMain(t *testing.T) {
	desc := "comparing 1 and 1"
	if 1 != 1 {
		t.Errorf(
			"%s failed, want:\n%s, got:\n%s",
			desc,
			1,
			1,
		)
	}
}
