package = "{{name}}"
version = "0.1-0"
rockspec_format = "3.0"
source = {
    url = "{{repo}}/{{name}}"
}
description = {
    license = "{{license}}",
}
dependencies = {
    "lua >= 5.1",
    "tl",
}
build = {
    type = "builtin",
    modules = {
        ["{{name}}"] = "build/init.lua",
        ["{{name}}.module"] = "build/module.lua",
    }
}
test = {
    type = "command",
    script = "./test/init.lua",
    test_dependencies = {
        "inspect >= 3.1.3"
    },
}
