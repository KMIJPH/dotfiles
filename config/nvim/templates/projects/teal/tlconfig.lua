return {
    build_dir = "build",
    source_dir = "src",
    include_dir = { "src" },
    gen_target = "5.1",
    global_env_def = "vim",
}
