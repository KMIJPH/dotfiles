#!/usr/bin/env lua
-- File Name: teal.lua
-- Description: Teal
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 24 May 2023 19:36:46
-- Last Modified: 16 Nov 2023 19:11:24

local TLDiag = require("nvim-utils.tl-diag")

local tld = TLDiag.new()
tld:run()
