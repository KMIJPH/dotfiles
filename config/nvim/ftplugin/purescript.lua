#!/usr/bin/env lua
-- File Name: purescript.lua
-- Description: Purescript things
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 02 May 2023 23:08:15
-- Last Modified: 16 Nov 2023 19:11:19

vim.api.nvim_cmd({ cmd = "setlocal", args = { "shiftwidth=2" } }, {})
