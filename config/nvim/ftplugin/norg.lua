#!/usr/bin/env lua
-- File Name: norg.lua
-- Description: Norg
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 26 Mar 2023 15:18:17
-- Last Modified: 16 Nov 2023 19:11:15

vim.api.nvim_cmd({ cmd = "setlocal", args = { "spell" } }, {})
vim.api.nvim_cmd({ cmd = "setlocal", args = { "foldlevel=0" } }, {})
