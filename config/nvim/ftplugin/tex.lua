#!/usr/bin/env lua
-- File Name: tex.lua
-- Description: Latex
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 26 Mar 2023 15:17:31
-- Last Modified: 16 Nov 2023 19:11:27

vim.api.nvim_cmd({ cmd = "setlocal", args = { "spell" } }, {})
vim.api.nvim_cmd({ cmd = "setlocal", args = { "textwidth=120" } }, {})
