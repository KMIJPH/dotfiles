#!/usr/bin/env lua
-- File Name: nix.lua
-- Description:
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 15 Nov 2023 18:58:49
-- Last Modified: 16 Nov 2023 19:11:11

vim.api.nvim_cmd({ cmd = "setlocal", args = { "shiftwidth=2" } }, {})
vim.api.nvim_cmd({ cmd = "setlocal", args = { "commentstring=#\\ %s" } }, {})
