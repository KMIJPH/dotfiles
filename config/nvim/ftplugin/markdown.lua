#!/usr/bin/env lua
-- File Name: markdown.lua
-- Description:
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 06 Apr 2024 23:18:53
-- Last Modified: 06 Apr 2024 23:19:01

vim.api.nvim_cmd({ cmd = "setlocal", args = { "spell" } }, {})
