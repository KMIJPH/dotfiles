#!/usr/bin/env lua
-- File Name: haskell.lua
-- Description:
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 15 Nov 2023 18:58:49
-- Last Modified: 29 Nov 2023 17:50:10

vim.api.nvim_cmd({ cmd = "setlocal", args = { "shiftwidth=2" } }, {})
