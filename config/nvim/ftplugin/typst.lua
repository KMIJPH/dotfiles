#!/usr/bin/env lua
-- File Name: typst.lua
-- Description:
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 28 Jul 2024 14:23:41
-- Last Modified: 08 Sep 2024 18:10:47

vim.api.nvim_cmd({ cmd = "setlocal", args = { "commentstring=//\\ %s" } }, {})
vim.api.nvim_cmd({ cmd = "setlocal", args = { "shiftwidth=2" } }, {})
vim.api.nvim_cmd({ cmd = "setlocal", args = { "spell" } }, {})
