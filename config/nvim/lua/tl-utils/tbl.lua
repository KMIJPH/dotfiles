local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local pairs = _tl_compat and _tl_compat.pairs or pairs







local tbl = {}


function tbl.join(first, second)
   for k, v in pairs(second) do first[k] = v end
   return first
end


function tbl.keys(t)
   local keys = {}
   local n = 0

   for k, _ in pairs(t) do
      n = n + 1
      keys[n] = k
   end

   return keys
end


function tbl.values(t)
   local vals = {}
   local n = 0

   for _, v in pairs(t) do
      n = n + 1
      vals[n] = v
   end

   return vals
end


function tbl.extend(first, second)
   for k, v in pairs(second) do
      if not first[k] then
         first[k] = v
      end
   end
   return first
end

return tbl
