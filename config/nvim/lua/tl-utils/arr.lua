local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local math = _tl_compat and _tl_compat.math or math; local table = _tl_compat and _tl_compat.table or table







local arr = {}


function arr.join(first, second)
   local slen = #second

   for i = 1, slen do
      first[#first + 1] = second[i]
   end

   return first
end


function arr.unique(a)
   local hash = {}
   local res = {}
   local alen = #a

   for i = 1, alen do
      local v = a[i]
      if not hash[v] then
         res[#res + 1] = v
         hash[v] = true
      end
   end

   return res
end


function arr.map(a, fun)
   local t = {}
   local alen = #a

   for i = 1, alen do
      t[i] = fun(a[i])
   end

   return t
end


function arr.filter(a, fun)
   local filtered = {}
   local alen = #a

   for i = 1, alen do
      local v = a[i]
      if fun(v) then
         table.insert(filtered, v)
      end
   end

   return filtered
end


function arr.rep(value, n)
   local tbl = {}

   for _ = 1, n do
      table.insert(tbl, value)
   end

   return tbl
end


function arr.contains(a, value)
   local alen = #a

   for i = 1, alen do
      local v = a[i]
      if v == value then
         return true
      end
   end

   return false
end


function arr.min(a)
   local min = math.huge

   for i = 1, #a do
      if a[i] and a[i] < min then min = a[i] end
   end

   if min == math.huge then
      return nil
   else
      return min
   end
end


function arr.max(a)
   local max = (math.huge * -1)

   for i = 1, #a do
      if a[i] and a[i] > max then max = a[i] end
   end

   if max == (math.huge * -1) then
      return nil
   else
      return max
   end
end

return arr
