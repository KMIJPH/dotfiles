local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local io = _tl_compat and _tl_compat.io or io; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table







local SH = {}




function SH.new(cmd, ...)
   local self = setmetatable({}, { __index = SH })
   self.cmd = cmd
   self.args = { ... }
   return self
end


function SH:run()
   local args = (self.args ~= nil and { table.concat(self.args, " ") } or { "" })[1]
   local cmd = ("%s %s"):format(self.cmd, args)

   local process = io.popen(("%s 2>&1"):format(cmd))

   local stdout
   local ret

   if process then
      stdout = process:read("*all")
      ret = { process:close() }
   else
      stdout = ("Cannot open process '%s'"):format(cmd)
      ret = {
         false,
         "lua",
         1,
      }
   end

   return stdout, not not not ret[1], ret[3]
end

return SH
