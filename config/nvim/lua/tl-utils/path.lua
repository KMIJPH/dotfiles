local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local package = _tl_compat and _tl_compat.package or package; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table







local path = {}


function path.sep()
   return package.config:sub(1, 1)
end


function path.join(...)
   local s = path.sep()
   local args = { ... }
   local p = table.concat(args, s):gsub(("%s%s"):format(s, s), s)
   return p
end


function path.filename(p)
   local name = p:match("^.+[/\\](.+)$")
   return (name ~= "" and { name } or { nil })[1]
end


function path.basename(p)
   local name = p:match("^.+[/\\](.+)%..*$")
   return (name ~= "" and { name } or { nil })[1]
end


function path.extension(p)
   local ext = p:match("^.+%.(.+)$")
   return (ext ~= "" and { ext } or { nil })[1]
end


function path.parent(p)
   local par = p:match("^(.*)[//\\]")
   return (par ~= "" and { par } or { nil })[1]
end


function path.subdir(p, sub)
   local pattern = ("^(.+)/%s"):format(sub)
   local m = string.match(p, pattern)
   return m
end

return path
