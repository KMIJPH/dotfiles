local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table







local str = {}


function str.line_count(s)
   local count = 1
   for i = 1, #s do
      local c = s:sub(i, i)
      if c == '\n' then count = count + 1 end
   end
   return count
end


function str.split_lines(s)
   local lines = {}
   local current_line = {}
   local inside_quotes = false
   local i = 1

   while i <= #s do
      local char = s:sub(i, i)


      if char == '"' or char == "'" then
         inside_quotes = not inside_quotes
         table.insert(current_line, char)
      elseif char == "\\" and s:sub(i + 1, i + 1) == "n" and inside_quotes then

         table.insert(current_line, "\\n")
         i = i + 1
      elseif char == "\n" and not inside_quotes then

         table.insert(lines, table.concat(current_line))
         current_line = {}
      elseif char == "\n" and inside_quotes then
         table.insert(current_line, "\\n")
      else
         table.insert(current_line, char)
      end
      i = i + 1
   end

   if #current_line > 0 then
      table.insert(lines, table.concat(current_line))
   end

   return lines
end


function str.split(s, sep)
   local pieces = {}
   for piece in s:gmatch(("([^%s]+)"):format(sep)) do
      table.insert(pieces, piece)
   end

   return pieces
end


function str.trim(s)
   local r = s:gsub("[%s]+$", ""):gsub("^[%s]+", "")
   return r
end


function str.regex_escape(s)
   local escaped = s:gsub("[^%w]", "%%%1")
   return escaped
end


function str.escape(s, char, with)
   with = with or "\\"
   local r = s:gsub(char, ("%s%s"):format(with, char))
   return r
end


function str.startswith(s, s1)
   local m = s:match(("^%s.*"):format(s1))
   return m ~= nil
end


return str
