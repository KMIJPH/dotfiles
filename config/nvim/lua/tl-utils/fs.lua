local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local io = _tl_compat and _tl_compat.io or io; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local os = _tl_compat and _tl_compat.os or os; local pcall = _tl_compat and _tl_compat.pcall or pcall; local string = _tl_compat and _tl_compat.string or string







local str = require("tl-utils.str")
local path = require("tl-utils.path")
local SH = require("tl-utils.sh")
local arr = require("tl-utils.arr")

local fs = {}


function fs.temp()
   local temp_file = os.tmpname()
   pcall(os.remove, temp_file)

   local sep_idx = temp_file:reverse():find(path.sep())
   local temp_path_len = #temp_file - sep_idx
   return temp_file:sub(1, temp_path_len)
end


function fs.exists(p)
   local ok
   local msg

   ok, msg = os.rename(p, p)
   return not not ok, msg
end


function fs.isdir(p)
   return fs.exists(p .. path.sep())
end


function fs.mkdir(p)
   if not fs.isdir(p) then
      local output, err = SH.new("mkdir", "-p", p):run()
      if err then
         return output, false
      end
   end

   return "", true
end


function fs.readlink(p, flag)
   flag = flag or ""
   local output
   local err

   output, err = SH.new("readlink", flag, p):run()
   if err or output == "" then
      return p
   end
   local link = output:gsub("[\n]+$", "")
   return link
end



function fs.read(p)
   local file = io.open(fs.readlink(p, "-f"), "r")

   if file then
      local text = file:read("*a")
      file:close()
      return text, true
   end

   return ("could not read file '%s'"):format(p), false
end




function fs.write(p, s, append)
   local flag = (append == true and { "a" } or { "w" })[1]
   local file = io.open(fs.readlink(p, "-f"), flag)

   if file then
      file:write(s)
      file:close()
      return "", true
   else
      fs.mkdir(path.parent(p))
      file = io.open(fs.readlink(p, "-f"), flag)
      file:write(s)
      file:close()
      return "", true
   end

   return ("could not write file '%s'"):format(p), false
end


function fs.copy(src, dst, flag)
   flag = flag or ""

   local output, err = SH.new("cp", flag, src, dst):run()
   if err then
      return output, false
   end

   return "", true
end


function fs.ls(dir, flag)
   flag = flag or ""

   local output, err = SH.new("ls", flag, dir):run()
   if err or output == "" then
      return {}, false
   end

   local files = str.split_lines(output)
   local filtered = arr.filter(files, function(a) return (a ~= ".") and (a ~= "..") end)
   return filtered, true
end


function fs.tree(dir)
   local output, err = SH.new("find", dir):run()

   if err or output == "" then
      return {}, false
   end

   local files = str.split_lines(output)
   local filtered = arr.filter(files, function(a) return a ~= dir end)
   return filtered, true
end


function fs.rev_find(dir, file)
   local function find(files)
      if files then
         for _, f in ipairs(files) do
            if f:match(file) then
               return true
            end
         end
         return false
      end
   end

   if not file then
      return "", false
   end

   local files = fs.ls(dir, "-a")
   local match = find(files)

   if not match then
      if dir then
         return fs.rev_find(path.parent(dir), file)
      else
         return "", false
      end
   else
      return dir, true
   end
end

return fs
