-- github.com/nvim-lualine/lualine.nvim

local api       = vim.api
local lsp       = vim.lsp
local fn        = vim.fn

local nvim_util = require("nvim-utils.util")

--- returns current lsp server name
local function lspservername()
    local msg = "None"
    local buf_ft = api.nvim_buf_get_option(0, "filetype")
    local clients = lsp.get_active_clients()

    if next(clients) == nil then return msg end

    for _, client in ipairs(clients) do
        local filetypes = client.config.filetypes
        if filetypes and fn.index(filetypes, buf_ft) ~= -1 then
            return client.name
        end
    end

    return msg
end

--- returns line/word/character count
local function get_word_count()
    local wc = api.nvim_eval("wordcount()")
    local lines = api.nvim_eval("line('$')")
    local words, characters

    if wc["visual_words"] then
        local row_start, row_end = nvim_util.visual_selection()
        lines = ("%d"):format(row_end - row_start + 1)
        words = wc["visual_words"]
        characters = wc["visual_chars"]
    else
        words = wc["words"]
        characters = wc["chars"]
    end

    return ("%s󰦪 %s󰬞 %s󰬊"):format(lines, words, characters)
end

require("lualine").setup {
    options = {
        icons_enabled = true,
        theme = "codedark",
        component_separators = { left = "", right = "" },
        section_separators = { left = "", right = "" },
        disabled_filetypes = {},
        always_divide_middle = true,
    },
    sections = {
        lualine_a = { "mode" },
        lualine_b = {
            { "branch" },
            --             { "encoding" },
            { "filetype" },
        },
        lualine_c = {
            { "'%='" },
            {
                "filename",
                file_status = true,
                path = 3,
            },
        },
        lualine_x = {
            { lspservername },
            { "diagnostics", sources = { "nvim_diagnostic" } },
        },
        lualine_y = { "progress", get_word_count },
        lualine_z = { "location" }
    },
    tabline = {},
    extensions = {}
}
