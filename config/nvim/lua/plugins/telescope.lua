-- github.com/nvim-telescope/telescope.nvim#customization

require("telescope").setup {
    defaults = {
        path_display = { "truncate" },
        sorting_strategy = "ascending",
        color_devicons = true,
        disable_devicons = false,
        mappings = {
            i = {
                ["<C-c>"] = false,
            }
        }
    },
    extensions = {},
    pickers = {
        find_files = {
            find_command = {
                "rg",
                "--files",
                "--hidden",
                "--glob", "!**/.git/*",
                "--glob", "!**/pCloudDrive/*",
                "--glob", "!*.pdf",
                "--glob", "!*.png",
                "--glob", "!*.svg",
            },
        },
    },
}
