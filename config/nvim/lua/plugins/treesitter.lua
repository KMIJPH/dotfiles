-- github.com/nvim-treesitter/nvim-treesitter

local ts = require("nvim-treesitter.configs")

-- use haskell parser for purescript
-- vim.treesitter.language.register("haskell", "purescript")

ts.setup {
    ensure_installed = {
        "bash",
        "cmake",
        "dhall",
        "go",
        "haskell",
        "html",
        "javascript",
        "julia",
        "latex",
        "lua",
        "make",
        "markdown",
        "nix",
        "norg",
        "org",
        "purescript",
        "python",
        "rust",
        "toml",
        "typescript",
        "typst",
        "yaml",
        "zig",
    },
    highlight = {
        enable = true,
    },
    textobjects = {
        lsp_interop = {
            enable = true,
            border = "none",
            peek_definition_code = {
                ["<leader>df"] = "@function.outer",
                ["<leader>dF"] = "@class.outer",
            },
        },
    },
}

require("nvim-treesitter").setup()
