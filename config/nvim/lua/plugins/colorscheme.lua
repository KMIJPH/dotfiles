-- github.com/rebelot/kanagawa.nvim
-- :KanagawaCompile

require("kanagawa").setup({
    compile = true,
    undercurl = true,
    commentStyle = { italic = true },
    functionStyle = {},
    keywordStyle = { italic = true },
    statementStyle = { bold = true },
    typeStyle = {},
    transparent = false,
    dimInactive = false,
    theme = "wave",
    colors = {
        theme = {
            wave = {
                ui = {
                    bg = "#1c1c1c",
                    fg = "#bcc5d6",
                },
                float = {
                    bg = "#1c1c1c",
                    fg = "#bcc5d6",
                },
            }
        },
    },
})
