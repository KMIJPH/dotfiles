-- github.com/folke/lazy.nvim

return {
    {
        "rebelot/kanagawa.nvim",
        desc = "Colorscheme inspired by the colors of the famous painting by Katsushika Hokusai.",
        config = function()
            require("plugins.colorscheme")
        end,
    },
    {
        "nvim-treesitter/nvim-treesitter",
        desc = "Tree-sitter integration.",
        config = function()
            require("plugins.treesitter")
        end,
    },
    {
        "neovim/nvim-lspconfig",
        desc = "Language server protocol configuration.",
        dependencies = {
            "stevearc/conform.nvim",
        },
        config = function()
            require("plugins.lsp")
        end,
    },
    {
        "hrsh7th/nvim-cmp",
        desc = "Completion engine.",
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "L3MON4D3/LuaSnip",
        },
        config = function()
            require("plugins.cmp")
        end,
    },
    {
        "nvim-telescope/telescope.nvim",
        desc = "Fuzzy finder over lists. Includes file browser.",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        config = function()
            require("plugins.telescope")
        end,
    },
    {
        "purescript-contrib/purescript-vim",
        desc = "Purescript indent, syntax, etc.",
    },
    {
        "teal-language/vim-teal",
        desc = "Teal syntax, etc.",
    },
    {
        "hoob3rt/lualine.nvim",
        desc = "Statusline (bottom).",
        dependencies = {
            "kyazdani42/nvim-web-devicons",
        },
        config = function()
            require("plugins.lualine")
        end,
    },
    {
        "kdheepak/tabline.nvim",
        desc = "Tabline (top).",
        dependencies = {
            "hoob3rt/lualine.nvim",
            "kyazdani42/nvim-web-devicons",
        },
        config = function()
            require("plugins.tabline")
        end,
    },
    {
        "lukas-reineke/indent-blankline.nvim",
        desc = "Indentation guides for lines.",
        config = function()
            require("ibl").setup {
                indent = { char = "¦" },
            }
        end,
    },
    {
        "windwp/nvim-autopairs",
        desc = "Autopair plugin.",
        config = function()
            require("nvim-autopairs").setup({
                enable_check_bracket_line = false,
                ignored_next_char = "[%w%.]",
                check_ts = true,
            })
        end,
    },
    {
        "L3MON4D3/LuaSnip",
        desc = "Snippets",
        lazy = false,
        dependencies = { "saadparwaiz1/cmp_luasnip" },
        keys = {
            {
                "<C-.>",
                function() require("luasnip").jump(1) end,
                desc = "Jump forward a snippet placement",
                mode = "i",
                noremap = true,
                silent = true
            },
            {
                "<C-:>,",
                function() require("luasnip").jump(-1) end,
                desc = "Jump backward a snippet placement",
                mode = "i",
                noremap = true,
                silent = true
            }
        },
        config = function()
            require("luasnip.loaders.from_vscode").load({ paths = { "./snippets/" } })
        end
    },
}
