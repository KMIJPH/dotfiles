-- github.com/neovim/nvim-lspconfig

local lsp_conf     = require("lspconfig")
local lsp_util     = require("lspconfig/util")
local api          = vim.api
local util         = require("tl-utils")

local venv_path    = util.path.join(vim.fn.stdpath("data"), "mypy", "python_venv")
local python_path  = util.path.join(vim.fn.stdpath("data"), "mypy", "python_path")
local dmypy_status = util.path.join(vim.fn.stdpath("data"), "mypy", "dmypy.json")
local mypy_cache   = util.path.join(vim.fn.stdpath("data"), "mypy", "cache")

require("conform").setup({
    formatters_by_ft = {
        sh = { "shfmt" },
        latex = { "latexindent" },
        typst = { "typstyle" },
        julia = { "julia", "-e", "'using JuliaFormatter; print(format_text(String(read(stdin))))'" },
    },
    format_on_save = {
        asnyc = true,
        lsp_format = "fallback",
    },
})

-- PYTHON
lsp_conf.pylsp.setup {
    root_dir = lsp_util.root_pattern("pyproject.toml"),
    on_init = function(client)
        local path, match = util.fs.rev_find(client.config.root_dir, ".venv")
        if match == true then
            -- writing files, since it won't work otherwise
            local venv = util.path.join(path, ".venv")
            local python = util.path.join(venv, "bin", "python")
            util.fs.write(venv_path, venv)
            util.fs.write(python_path, python)
        end
    end,
    cmd_env = { VIRTUAL_ENV = util.fs.read(venv_path) },
    settings = {
        pylsp = {
            plugins = {
                rope = { enabled = true, ropeFolder = nil },
                autopep8 = { enabled = true },
                pyflakes = { enabled = false },
                mccabe = { enabled = false },
                pycodestyle = { enabled = false },
                pydocstyle = { enabled = false },
                flake8 = { enabled = false },
                pylint = { enabled = false },
                yapf = { enabled = false },
                ruff = {
                    enabled = true,
                    extendSelect = { "I" },
                    ignore = { "I001" },
                    -- lineLength = 200,
                },
                pylsp_mypy = {
                    enabled = true,
                    dmypy = true,
                    live_mode = false,
                    strict = false,
                    dmypy_status_file = dmypy_status,
                    overrides = {
                        "--python-executable", util.fs.read(python_path),
                        "--cache-dir", mypy_cache,
                        true
                    }
                }
            }
        }
    }
}

-- GOLANG
lsp_conf.gopls.setup({
    cmd = { "gopls" },
})

-- RUST
lsp_conf.rust_analyzer.setup({
    cmd = { "rust-analyzer" },
    filetypes = { "rust" }
})

-- BASH
lsp_conf.bashls.setup({
    cmd = { "bash-language-server", "start" },
})

-- LUA
lsp_conf.lua_ls.setup({
    cmd = { "lua-language-server" },
    root_dir = function(filepath)
        return lsp_util.root_pattern(
            ".luarc.json", ".luarc.jsonc", ".luacheckrc", ".stylua.toml", "stylua.toml", "selene.toml", "selene.yml",
            ".git", ".config/nvim"
        )(filepath)
    end,
    settings = {
        Lua = {
            runtime = {
                version = "LuaJIT",
                path = { "?.lua", "/src/?.lua" }
            },
            diagnostics = {
                globals = { "vim" },
            },
            workspace = {
                library = {
                    api.nvim_get_runtime_file("", true),
                },
                checkThirdParty = false,
            },
            telemetry = {
                enable = false,
            },
        },
    },
})

-- PURESCRIPT
lsp_conf.purescriptls.setup {
    filetypes = { "purescript" },
    cmd = { "purescript-language-server", "--stdio" },
    root_dir = function(filepath)
        if filepath:match("/.spago/") then
            return nil
        end
        return lsp_util.root_pattern("bower.json", "psc-package.json", "spago.dhall", "flake.nix", "shell.nix")(filepath)
    end,
    settings = {
        purescript = {
            formatter = "purs-tidy"
        }
    }
}

-- NIX
lsp_conf.nil_ls.setup {
    filetypes = { "nix" },
    cmd = { "nil" },
    single_file_support = true,
    settings = {
        ['nil'] = { formatting = { command = { "nixfmt" } } }
    }
}

-- JULIA
-- julia --project=~/.julia/environments/nvim-lspconfig -e 'using Pkg; Pkg.add("LanguageServer")'
-- julia --project=/path/to/my/project -e 'using Pkg; Pkg.instantiate()'
lsp_conf.julials.setup({})

-- JAVASCRIPT / TYPESCRIPT
lsp_conf.ts_ls.setup({
    cmd = { "typescript-language-server", "--stdio" },
})

-- HASKELL
-- don't forget to run cabal update (and set the correct base version in *.cabal)
lsp_conf.hls.setup {
    filetypes = { "haskell", "lhaskell", "cabal" },
    cmd = { "haskell-language-server-9.6.5", "--lsp" },
    root_dir = function(filepath)
        return (
            lsp_util.root_pattern("hie.yaml", "stack.yaml", "cabal.project", "*.cabal", "package.yaml")(filepath)
        )
    end,
    settings = {
        haskell = {
            cabalFormattingProvider = "cabal-fmt",
            formattingProvider = "ormolu"
        }
    }
}

-- TYPST
lsp_conf.tinymist.setup({
    cmd = { "tinymist" },
    filetypes = { "typst" },
    offset_encoding = "utf-8",
    settings = {
        exportPdf = "never", -- onType, onSave or never
    }
})

-- ZIG
lsp_conf.zls.setup({})
