-- github.com/nvim-neorg/neorg

require("neorg").setup {
    load = {
        ["core.defaults"] = {},
        ["core.concealer"] = {},
        -- ["core.summary"] = {},
        -- ["core.pivot"] = {},
        -- ["core.itero"] = {},
        -- ["core.promo"] = {},
        ["core.export"] = {},
        ["core.export.markdown"] = {},
        ["core.completion"] = {
            config = { engine = "nvim-cmp" },
        },
        ["core.integrations.nvim-cmp"] = {},
        ["core.integrations.treesitter"] = {},
        ["core.keybinds"] = {
            config = {
                neorg_leader = " ",
            }
        },
        ["core.dirman"] = {
            config = {
                workspaces = {
                    notes = "~/Sync/scripts/norg",
                    lists = "~/Sync/scripts/norg/lists",
                    manuals = "~/Sync/scripts/norg/manuals",
                },
                default_workspace = "notes",
            }
        },
        ["core.journal"] = {
            config = {
                workspace = "notes",
                journal_folder = "journal",
                strategy = "flat",
            }
        },
        -- ["core.ui.calendar"] = {},
    }
}
