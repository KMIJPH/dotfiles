#!/usr/bin/env lua
-- File Name: global.lua
-- Description: Neovim global settings
-- Docs: neovim.io/doc/user/options.html
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 02 Oct 2022 18:32:12
-- Last Modified: 04 Jul 2024 20:44:27

local api  = vim.api
local g    = vim.g
local gopt = vim.o
local bopt = vim.bo
local wopt = vim.wo

--CMDs------------------------
api.nvim_cmd(
    {
        cmd = "filetype",
        args = { "plugin", "indent", "on" },
    }, {}
--     { desc = "file type identification, plugin and indenting" }
)
api.nvim_cmd(
    {
        cmd = "syntax",
        args = { "enable" },
    }, {}
--     { desc = "enable syntax highlighting" }
)
api.nvim_cmd(
    {
        cmd = "set",
        args = { "langmenu=en_US" },
    }, {}
--     { desc = "set language" }
)
api.nvim_cmd(
    {
        cmd = "colorscheme",
        args = { "kanagawa" }
    }, {}
--     { desc = "set colorscheme" }
)

--GLOBAL----------------------
g.mapleader         = " "                             -- set space as leader key
gopt.autochdir      = true                            -- change directory to the file in the current window
gopt.autoindent     = true                            -- take indent for new line from previous line
gopt.clipboard      = "unnamedplus"                   -- use the clipboard as the unnamed register
gopt.cmdheight      = 0                               -- number of lines to use for the command-line
gopt.colorcolumn    = "125"                           -- highlight column
gopt.completeopt    = "menuone,noinsert,noselect"     -- options for Insert mode completion
gopt.concealcursor  = "nc"                            -- something about cursor?
gopt.conceallevel   = 0                               -- whether concealable text is shown or hidden
gopt.confirm        = true                            -- ask what to do about unsaved/read-only files
gopt.cursorline     = true                            -- highlight the screen line of the cursor
gopt.encoding       = "utf-8"                         -- encoding used internally
gopt.expandtab      = true                            -- use spaces when <Tab> is inserted
gopt.fileencoding   = "utf-8"                         -- file encoding for multi-byte text
gopt.foldlevelstart = 99                              -- Open folds by default
gopt.guifont        = "Monoid_Nerd_Font_Complete:h12" -- Name(s) of font(s) to be used
gopt.hidden         = true                            -- don't unload buffer when it is abandoned
gopt.ignorecase     = true                            -- ignore case in search patterns
gopt.inccommand     = "split"                         -- Preview of |:substitute|, |:smagic|, and |:snomagic|.
gopt.joinspaces     = false                           -- two spaces after a period with a join command
gopt.laststatus     = 3                               -- global status line
gopt.list           = true                            -- enable the characters below (i guess)
gopt.listchars      = "tab:> ,trail:_"                -- set tab, trailing and end of line (eol) characters
gopt.mouse          = "a"                             -- enable the use of mouse clicks
gopt.number         = true                            -- print the line number in front of each line
gopt.pumheight      = 10                              -- maximum height of the popup menu
gopt.relativenumber = true                            -- prints relative numbers
gopt.ruler          = true                            -- show cursor line and column in the status line
gopt.scrolloff      = 10                              -- minimum nr. of lines above and below cursor
gopt.shiftround     = true                            -- round indent to multiple of shiftwidth
gopt.shiftwidth     = 4                               -- number of spaces to use for (auto)indent step
gopt.shortmess      = "aOWTF"                         -- list of flags, reduce length of messages
gopt.showmode       = false                           -- message on status line to show current mode
gopt.showtabline    = 2                               -- tells when the tab pages line is displayed
gopt.sidescrolloff  = 8                               -- min. nr. of columns to left and right of cursor
gopt.signcolumn     = "yes"                           -- NVIM: When and how to draw the signcolumn
gopt.smartcase      = true                            -- no ignore case when pattern has uppercase
gopt.smartindent    = true                            -- smart autoindenting for C programs
gopt.smarttab       = true                            -- use 'shiftwidth' when inserting <Tab>
gopt.spelllang      = "en,de,it"                      -- spell checking for these languages
gopt.spellsuggest   = "best,9"                        -- spell suggestions
gopt.splitbelow     = true                            -- new window from split is below the current one
gopt.splitright     = true                            -- new window is put right of the current one
gopt.swapfile       = false                           -- whether to use a swapfile for a buffer
gopt.undofile       = true                            -- whether to store edit history in files
gopt.undodir        = ("%s/undo"):format(vim.fn.stdpath("data"))
gopt.tabstop        = 4                               -- number of spaces that <Tab> in file uses
gopt.termguicolors  = true                            -- NVIM: Enables 24-bit RGB color in the TUI
gopt.timeoutlen     = 300                             -- Time in milliseconds to wait for a mapped sequence to complete.
gopt.title          = true                            -- let neovim set the title of the window
gopt.titlestring    = "Neovim: %t"                    -- string to use for the Vim window title
gopt.updatetime     = 100                             -- after this many milliseconds flush swap file
gopt.wildmode       = "list:longest"                  -- mode for 'wildchar' command-line expansion

--WINDOW----------------------
wopt.list           = true     -- show <Tab> and <EOL>
wopt.wrap           = false    -- long lines wrap and continue on the next line
wopt.foldmethod     = "manual" -- set default foldmethod

--BUFFER----------------------
bopt.iskeyword      = "@,48-57,_,192-255,-" -- treat strings containing these patters as words
