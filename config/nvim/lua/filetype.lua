#!/usr/bin/env lua
-- File Name: filetype.lua
-- Description: Filetype settings
-- Docs: neovim.io/doc/user/lua.html
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 01 Jun 2023 16:06:46
-- Last Modified: 10 Jul 2024 09:45:54

vim.filetype.add({
    extension = {
        tex = "tex", -- instead of plaintex
        typ = "typst",
    },
})
