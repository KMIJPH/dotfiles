#!/usr/bin/env lua
-- File Name: mappings.lua
-- Description: Neovim mappings
-- Docs: neovim.io/doc/user/map.html
-- Dependencies:
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 02 Oct 2022 18:32:12
-- Last Modified: 12 Sep 2024 16:43:54

local lsp = vim.lsp
local util = require("nvim-utils.util")
local map = util.map

--MISC------------------------
map('n', '<Leader>1', ':set <C-R>=&conceallevel ? "conceallevel=0" : "conceallevel=3"<CR><CR>',
    { desc = 'Toggles conceal level.' })
map('', '<Leader>x', ':noh<CR>', { desc = 'Clears highlights.' })
map('n', '<F1>', ':Telescope help_tags<CR>', { desc = 'Opens nvim help in a Telescope list.' })
map('i', '<F1>', '<nop>', { desc = 'Unmaps Help.' })
map('n', 'q', '<nop>', { desc = 'Unmaps Macro recording.' })
map('i', '<PageUp>', '<nop>', { desc = 'Unmaps PageUp.' })
map('i', '<PageDown>', '<nop>', { desc = 'Unmaps PageDown.' })
map('i', '<Home>', '<nop>', { desc = 'Unmaps Home.' })
map('i', '<End>', '<nop>', { desc = 'Unmaps End.' })
map('n', '<PageUp>', '<nop>', { desc = 'Unmaps PageUp.' })
map('n', '<PageDown>', '<nop>', { desc = 'Unmaps PageDown.' })
map('n', '<Home>', '<nop>', { desc = 'Unmaps Home.' })
map('n', '<End>', '<nop>', { desc = 'Unmaps End.' })
map('n', '<Leader>gl', ':NvimGotoLine<CR>', { desc = 'Goto line/column.' })

--BUFFERS---------------------
map('n', '<S-TAB>', ':bprev<CR>', { desc = "Moves to previous buffer." })
map('n', '<TAB>', ':bnext<CR>', { desc = "Moves to next buffer." })
map('n', '<Leader>gt', ':REPLSwitchToTerm<CR>', { desc = "Moves to terminal window." })
map('n', '<Leader>gb', ':REPLSwitchToWin<CR>', { desc = "Moves to non terminal window." })
map('n', '<Leader>q', ':REPLCloseBuf<CR>', { desc = "Closes buffer." })
map('n', '<C-s>', ':w<CR>', { desc = "Saves current file." })
map('n', '<Leader>fn', ':NvimBufAdd<CR>', { desc = 'Creates a new file in a new buffer.' })
map('n', '<F2>', function() lsp.buf.hover() end, { desc = "Shows documentation for item under cursor." })
map('n', '<F3>', function() lsp.buf.signature_help() end, { desc = "Shows signature help for item under cursor." })
map('n', '<F4>', function() vim.diagnostic.open_float() end, { desc = "Shows lsp diagnostics for item under cursor." })

--NAVIGATION------------------
map('n', '<Leader>e', ':LF<CR>', { silent = true, desc = "Opens file browser." })
map('n', '<C-h>', '<C-w>h', { desc = "Moves to window on the left." })
map('n', '<C-j>', '<C-w>j', { desc = "Moves to window below." })
map('n', '<C-k>', '<C-w>k', { desc = "Moves to window above." })
map('n', '<C-l>', '<C-w>l', { desc = "Moves to window on the right." })
map('n', '<C-r>', '<C-w>r', { desc = "Swaps split positions." })

map('n', '<Leader>h', '<C-w>H', { desc = "Positions window to the left." })
map('n', '<Leader>j', '<C-w>J', { desc = "Positions window below." })
map('n', '<Leader>k', '<C-w>K', { desc = "Positions window above." })
map('n', '<Leader>l', '<C-w>L', { desc = "Positions window to the right." })

map('v', '<S-Up>', '<nop>', { desc = "Unmap up in visual mode." })
map('v', '<S-Down>', '<nop>', { desc = "Unmap down in visual mode." })

map('i', '<C-h>', '<Left>', { desc = "Moves cursor left in insert mode." })
map('i', '<C-j>', '<Down>', { desc = "Moves cursor down in insert mode." })
map('i', '<C-k>', '<Up>', { desc = "Moves cursor up in insert mode." })
map('i', '<C-l>', '<Right>', { desc = "Moves cursor right in insert mode." })
map('i', '<C-d>', '<C-o>x', { desc = "Delete in insert mode." })
map('i', '<C-e>', '<Esc>', { desc = 'Escape in insert mode.' })

map('n', '<M-j>', ':m . +1<CR>', { desc = "Moves current line down." })
map('n', '<M-k>', ':m . -2<CR>', { desc = "Moves current line up." })

--COPY, PASTE, ...------------
map('i', '<C-v>', '<C-r>+', { desc = "Pastes from clipboard." })
map('v', '<C-c>', '"+y', { desc = "Copies from clipboard." })
map('n', 'x', '"_x', { desc = "Don't let 'x' overwrite clipboard register." })
map('n', '<Del>', '"x', { desc = "Don't let <Del> overwrite clipboard register." })
map('n', '<BS>', '"_X', { desc = "Don't let <BS> overwrite clipboard register." })
map('n', 'cw', '"_cw', { desc = "Don't let 'cw' overwrite clipboard register." })
map('n', 'r', '<C-r>', { desc = "Redo." })
map('n', '<C-a>', 'ggVG', { desc = "Select all." })
map('v', '<S-TAB>', '<gv', { desc = "Decrease selection indentation." })
map('v', '<TAB>', '>gv', { desc = "Increase selection indentation." })

--SEARCH & REPLACE------------
map('n', '<Leader>s', '*', { desc = "Searches for word under the cursor." })
map('n', '<Leader>r', ':%s/<C-r><C-w>//<Left>', { desc = "Replaces word under the cursor." })
map('v', '<Leader>s', '"hy:/<C-r>h<CR><Esc>N', { desc = "Searches for word in selection." })
map('v', '<Leader>r', '"hy:%s/<C-r>h//g<Left><Left>', { desc = "Replaces selected word." })

--REPL------------------------
map('n', '<Leader><CR>', ':REPLOpenREPL<CR>', { desc = "Opens/toggles REPL window." })
map('n', '<Leader>$', ':REPLOpenTerm<CR>', { desc = "Opens terminal (shell) window." })
map('n', '<Leader><BS>', ':REPLCloseTerm<CR>', { desc = "Closes terminal window." })
map('n', '<C-Space>', ':REPLSendLine<CR>', { desc = "Sends line under cursor to the terminal/REPL." })
map('v', '<C-Space>', ':REPLSendSelection<CR>', { desc = "Sends selection to the terminal/REPL." })
map('n', '<C-B>', ':REPLRunFile<CR>', { desc = "Runs current file in the terminal (shell)." })
map('n', '<C-T>', ':REPLTest<CR>', { desc = "Runs tests in the terminal (shell)." })

--TELESCOPE------------
map('n', '<Leader>ff', ':Telescope find_files<CR>', { silent = true, desc = "Use Telescope to find files." })
map('n', '<Leader>gg', ':Telescope live_grep<CR>', { silent = true, desc = "Live grep using Telescope." })
map('n', '<Leader>t', ':Telescope oldfiles<CR>', { silent = true, desc = "List file history using Telescope." })
map('n', '<Leader>l', ':TelescopeLsp<CR>', { desc = 'Shows lsp actions in telescope.' })
