local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local string = _tl_compat and _tl_compat.string or string







local api = vim.api
local nvim_util = require("nvim-utils.util")

local REPL = {REPLOpts = {}, }
















function REPL.new(opts)
   local self = setmetatable({}, { __index = REPL })
   self.opts = opts
   return self
end

function REPL:next_win()
   local windows = api.nvim_list_wins()
   for _, win in ipairs(windows) do
      if win ~= self.win then
         return win
      end
   end
   return nil
end

function REPL:is_open()
   local windows = api.nvim_list_wins()
   for _, win in ipairs(windows) do
      if win == self.win then
         return true
      end
   end
   return false
end

function REPL:create()
   if not self:is_open() then
      api.nvim_cmd({ cmd = "split" }, {})
      api.nvim_cmd({ cmd = "terminal" }, {})
      api.nvim_cmd({ cmd = "set", args = { "nobuflisted" } }, {})
      self.win = api.nvim_get_current_win()
      api.nvim_win_set_height(self.win, self.opts.height)
   else
      api.nvim_set_current_win(self.win)
      api.nvim_win_set_height(self.win, self.opts.height)
   end
end

function REPL:kill()
   if self:is_open() then
      local buf_id = api.nvim_win_get_buf(self.win)
      api.nvim_buf_delete(buf_id, { force = true })
   end
   self.win = nil
end

function REPL:send(text)
   if self:is_open() then
      local buf_id = api.nvim_win_get_buf(self.win)
      local chan_id = api.nvim_buf_get_option(buf_id, "channel")
      api.nvim_chan_send(chan_id, ("%s\n"):format(text))

      local line_count = api.nvim_buf_line_count(buf_id)
      api.nvim_win_set_cursor(self.win, { line_count, 0 })
   else
      print("Couldn't find any open terminal.")
   end
end

function REPL:open(repl_mode)
   local filetype = api.nvim_buf_get_option(0, "filetype")
   local win_id = self:next_win()

   self:create()

   if win_id then
      api.nvim_set_current_win(win_id)
   end

   if repl_mode == true then
      local command = self.opts.repl_cmds[filetype]
      if command == nil then
         print(("No REPL command found for filetype %s. Opening terminal."):format(filetype))
         if self:is_open() then
            api.nvim_set_current_win(self.win)
         end
         api.nvim_cmd({ cmd = "startinsert" }, {})
      else
         local response = command()

         if response then
            self:send(response)
         end
      end
   else
      if self:is_open() then
         api.nvim_set_current_win(self.win)
      end
   end
end

function REPL:minimize()
   if self:is_open() then
      if api.nvim_win_get_height(self.win) == 1 then
         api.nvim_win_set_height(self.win, self.opts.height)
      else
         api.nvim_win_set_height(self.win, 1)
      end
      api.nvim_set_current_win(self:next_win())
   else
      self:open(true)
   end
end

function REPL:toggle()
   if self.win == nil then
      self:open(true)
   else
      self:minimize()
   end
end

function REPL:send_line()
   local line = api.nvim_get_current_line()
   self:send(line)
   vim.api.nvim_input("j^")
end

function REPL:send_selection()
   local lines = nvim_util.buf.vline()

   for _, line in ipairs(lines) do
      if line ~= "" then
         self:send(line)
      end
   end
end

function REPL:run_file(t)
   local filetype = api.nvim_buf_get_option(0, "filetype")
   local file = api.nvim_buf_get_name(0)

   local command

   if t == "compile" then
      command = self.opts.compile_cmds[filetype]
   elseif t == "test" then
      command = self.opts.test_cmds[filetype]
   end

   if command == nil then
      print(("No %s command found for filetype %s."):format(t, filetype))
   else
      local response = command(file)
      if response then
         if not self:is_open() then
            print("No terminal open. Opening.")
            self:open(false)
            api.nvim_cmd({ cmd = "startinsert" }, {})
         end
         self:send(response)
      end
   end
end

function REPL:close()
   local curr = api.nvim_get_current_buf()
   local curr_opts = vim.fn.getbufinfo(curr)
   if #curr_opts > 0 and curr_opts[1].listed == 0 then
      api.nvim_cmd({ cmd = "bw" }, {})
      return
   end

   local opts = vim.fn.getbufinfo()
   for i = #opts, 1, -1 do
      local o = opts[i]
      if o.listed == 1 and o.bufnr ~= curr then
         api.nvim_set_current_buf(o.bufnr)
         api.nvim_buf_delete(curr, {})
         return
      end
   end
end

function REPL:switch_to_term()
   api.nvim_set_current_win(self.win)
   api.nvim_cmd({ cmd = "startinsert" }, {})
end

function REPL:switch_to_win()
   local win_id = self:next_win()
   api.nvim_set_current_win(win_id)
end

return REPL
