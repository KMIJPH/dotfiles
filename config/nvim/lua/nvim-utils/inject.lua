local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then
    local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end
end; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table







local nvim_util = require("nvim-utils.util")
local util = require("tl-utils")

local Injector = { Source = {}, InjectOpts = {}, }



















function Injector.new(opts)
    local self = setmetatable({}, { __index = Injector })

    if not opts.template_dir then
        error("Error: inject.tl: no template_dir set")
    end

    if not opts.packages then
        error("Error: inject.tl: no packages set")
    end

    self.opts = opts
    return self
end

function Injector:latex(file)
    local content, ok = util.fs.read(util.path.join(self.opts.template_dir, file))
    local packages, ok_p = util.fs.read(self.opts.packages)
    local lines = {}
    local function removeEmpty(list)
        local newList = {}
        local nonEmptyFound = false
        for i = #list, 1, -1 do
            if list[i] ~= "" then
                nonEmptyFound = true
            end
            if nonEmptyFound then
                table.insert(newList, 1, list[i])
            end
        end
        return newList
    end

    if not ok then
        error(("Error: inject.tl: could not read template file '%s'"):format(file))
    end

    if ok_p then
        local left, _ = nvim_util.comment_str()
        if not left then
            error("Error: inject.tl: could not find commentstring for current buffer")
        end
        packages = util.str.trim(packages)
        content = content:gsub(("%s PACKAGES_GO_HERE"):format(left), packages)
        lines = util.str.split_lines(util.str.trim(content))
    else
        error(("Error: inject.tl: could not read packages file '%s'"):format(self.opts.packages))
    end

    return removeEmpty(lines)
end

function Injector:inject(source)
    local content

    if source.file then
        content = self:latex(source.file)
    elseif source.snippet then
        local text = source.snippet
        content = util.str.split_lines(text)
        content = util.arr.filter(content, function(x) return x ~= "" end)
    else
        error("Error: inject.tl: no source file or snipped provided")
    end

    if source.pos == "top" then
        nvim_util.buf.insert_top(content)
    elseif source.pos == "cursor" then
        nvim_util.buf.insert_cursor(content)
    else
        error("Error: inject.tl: invalid position")
    end
end

function Injector:selection(macro_start, macro_end)
    local rs, re, cs, ce = nvim_util.visual_selection()

    if ce < 1000 then
        local lines = nvim_util.buf.visual()
        lines[1] = ("%s%s"):format(macro_start, lines[1])
        lines[#lines] = ("%s%s"):format(lines[#lines], macro_end)

        nvim_util.buf.set_text(rs, re, cs - 1, ce, lines)
    elseif ce >= 1000 then
        local lines = nvim_util.buf.vline()
        local indent = lines[1]:match("^(%s+).*")

        if indent then
            lines[1] = ("%s%s%s"):format(indent, macro_start, string.sub(lines[1], #indent, -1))
        else
            lines[1] = ("%s%s"):format(macro_start, lines[1])
        end

        lines[#lines] = ("%s%s"):format(lines[#lines], macro_end)
        nvim_util.buf.set_lines(rs, re, lines)
    end
end

return Injector
