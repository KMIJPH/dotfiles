local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local os = _tl_compat and _tl_compat.os or os; local string = _tl_compat and _tl_compat.string or string







local api = vim.api
local unmap = vim.keymap.del
local util = require("tl-utils")
local Float = require("nvim-utils.float")
local nvim_util = require("nvim-utils.util")

local LF = {LFOpts = {}, }









function LF.new(opts)
   local self = setmetatable({}, { __index = LF })

   if not opts.temp_path then
      error("Error: lf.tl: no temp_path set")
   end
   if not opts.config then
      error("Error: lf.tl: no config set")
   end
   if not opts.unmaps then opts.unmaps = {} end

   self.opts = opts
   return self
end

function LF:run()
   local f = Float.new()
   f:open()
   f:insert()
   f:add_autocmd(
   "TermOpen",
   function()
      vim.schedule(function()
         local maps = api.nvim_buf_get_keymap(f.buf, "t")
         for _, m in ipairs(maps) do
            for _, u in ipairs(self.opts.unmaps) do
               if m["lhs"] == u then
                  unmap("t", u, { buffer = f.buf })
               end
            end
         end

         nvim_util.map('t', '<ScrollWheelDown>', '<nop>', { desc = 'Unmaps Scroll.' })
         nvim_util.map('t', '<ScrollWheelUp>', '<nop>', { desc = 'Unmaps Scroll.' })
      end)
   end,
   "Disable some keys",
   { pattern = "lf*", target = "pattern" })

   f:add_autocmd(
   "TermClose",
   function()
      vim.schedule(function()
         local key = api.nvim_replace_termcodes("<CR>", true, false, true)
         api.nvim_feedkeys(key, "n", false)
      end)
   end,
   "Closes without process message.",
   { pattern = "lf*", target = "pattern" })

   f:add_autocmd(
   "WinClosed",
   function()
      if util.fs.exists(self.opts.temp_path) then
         local path = util.fs.read(self.opts.temp_path)
         os.remove(self.opts.temp_path)
         vim.schedule(function()
            api.nvim_cmd({ cmd = "edit", args = { path }, magic = { file = false, bar = false } }, {})
         end)
      end
   end,
   "Opens file in neovim upon selection.",
   { pattern = tostring(f.win), target = "pattern" })

   api.nvim_cmd(
   { cmd = "terminal", args = { ("lf -config=%s -selection-path=%s"):format(self.opts.config, self.opts.temp_path) } },
   {})

end

return LF
