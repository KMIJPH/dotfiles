return {
    comment = require("nvim-utils.comment"),
    util = require("nvim-utils.util"),
    Header = require("nvim-utils.header"),
    TlDiag = require("nvim-utils.tl-diag"),
    Injector = require("nvim-utils.inject"),
    Float = require("nvim-utils.float"),
    LG = require("nvim-utils.lazygit"),
    LF = require("nvim-utils.lf"),
    Project = require("nvim-utils.project"),
    nav = require("nvim-utils.nav"),
    REPL = require("nvim-utils.repl"),
}
