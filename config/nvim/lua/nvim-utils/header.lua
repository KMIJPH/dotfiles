local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then
    local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end
end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local string = _tl_compat and _tl_compat.string or string; local table =
    _tl_compat and _tl_compat.table or table







local api = vim.api
local au = api.nvim_create_autocmd
local util = require("tl-utils")
local nvim_util = require("nvim-utils.util")

local Header = { Field = {}, CommentString = {}, HeaderOpts = {}, }
































local function read_template(template)
    local content, ok = util.fs.read(template)
    local lines = {}

    if content then
        lines = util.str.split_lines(content)
    end

    return lines, ok
end


local function create(opts, template)
    local lines = {}

    if opts.prefix and #opts.prefix > 0 then
        util.arr.join(lines, opts.prefix)
    end

    for i, line in ipairs(template) do
        if opts.fields then
            for _, field in ipairs(opts.fields) do
                if field.event == "on_create" then
                    local match = line:match(("(.-%s).*"):format(field.field))

                    if match then
                        template[i] = ("%s %s"):format(match, field.replace())
                    end
                end
            end

            if template[i] ~= "" then
                if opts.cs.left then
                    template[i] = ("%s %s"):format(opts.cs.left, template[i])
                    if opts.cs.right then
                        template[i] = ("%s %s"):format(template[i], opts.cs.right)
                    end
                end

                table.insert(lines, template[i])
            end
        end
    end

    if opts.suffix and #opts.suffix > 0 then
        util.arr.join(lines, opts.suffix)
    end

    table.insert(lines, "")
    nvim_util.buf.insert_top(lines)
end

function Header.new(opts)
    local self = setmetatable({}, { __index = Header })

    if not opts.extensions then
        error("Error: header.tl: no extensions set")
    end

    local template, ok = read_template(opts.path)
    if not ok then
        error(("Error: header.tl: could not read template '%s'"):format(opts.path))
    end

    if not opts.group then
        opts.group = api.nvim_create_augroup("tl_header", { clear = true })
    end

    if not opts.search then opts.search = 15 end

    self.opts = opts
    self.template = template
    return self
end

function Header:on_create()
    au(
        { "BufNewFile" },
        {
            pattern = self.opts.extensions,
            callback = function()
                create(self.opts, self.template)
            end,
            desc = "Adds header string to buffer.",
            group = self.opts.group,
        })
end

function Header:on_save()
    local function update()
        local header_lines = api.nvim_buf_get_lines(0, 0, self.opts.search, false)

        for i, line in ipairs(header_lines) do
            for _, field in ipairs(self.opts.fields) do
                if field.event == "on_save" then
                    local match = line:match(("(.-%s).*$"):format(field.field))

                    if match then
                        local replacement = ("%s %s"):format(match, field.replace())
                        if self.opts.cs.right then
                            replacement = ("%s %s"):format(replacement, self.opts.cs.right)
                        end
                        nvim_util.buf.set_lines(i, i, { replacement })
                    end
                elseif field.event == "on_create" then
                    local match, value = line:match(("(.-%s)(.*)$"):format(field.field))

                    if match and value == "" then
                        local replacement = ("%s %s"):format(match, field.replace())
                        if self.opts.cs.right then
                            replacement = ("%s %s"):format(replacement, self.opts.cs.right)
                        end
                        nvim_util.buf.set_lines(i, i, { replacement })
                    end
                end
            end
        end
    end

    au(
        { "BufWritePre", "FileWritePre" },
        {
            pattern = self.opts.extensions,
            callback = function()
                update()
            end,
            desc = "Updates header on save.",
            group = self.opts.group,
        })
end

function Header.add(path, fields)
    local template, ok = read_template(path)
    if not ok then
        error(("Error: header.tl: could not read template '%s'"):format(path))
    end

    local left, right = nvim_util.comment_str()

    if left then
        local opts = {
            cs = { left = left, right = right },
            fields = fields,
        }
        create(opts, template)
    end
end

return Header
