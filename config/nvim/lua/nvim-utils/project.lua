local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local os = _tl_compat and _tl_compat.os or os; local string = _tl_compat and _tl_compat.string or string







local api = vim.api
local util = require("tl-utils")
local nvim_util = require("nvim-utils.util")
local Float = require("nvim-utils.float")







local Project = {ProjOpts = {}, }













local function parse_input(input)
   local inserted = util.arr.filter(
   input,
   function(l)
      return not (l == "") and not (l:match("^[%s]+$") == true)
   end)


   local parsed = {}
   if #inserted >= 2 then
      parsed.dir = inserted[1]
      parsed.language = inserted[2]
   end
   if #inserted > 2 then
      parsed.license = inserted[3]
   end

   return parsed
end


local function fill(s, parsed, opts)
   s = s:gsub("{{name}}", util.path.filename(parsed.dir))
   s = s:gsub("{{year}}", os.date("%Y"))
   s = s:gsub("{{repo}}", opts.repo)
   s = s:gsub("{{author}}", opts.author)
   s = s:gsub("{{license}}", parsed.license)
   s = s:gsub("{{version}}", opts.version)
   return s
end


local function fill_template(src, dst, parsed, opts)
   local content, ok = util.fs.read(src)
   if ok then
      content = fill(content, parsed, opts)
      local msg, ok2 = util.fs.write(dst, content)
      if not ok2 then
         print(msg)
      end
   end
end


local function create_template(dst, parsed, opts)
   local template_dir = util.path.join(opts.template_path, parsed.language)
   local tree, ok = util.fs.tree(template_dir)
   if ok then
      for _, p in ipairs(tree) do
         if util.fs.isdir(p) then
            local dst_path = p:gsub(template_dir, dst)
            util.fs.mkdir(dst_path)
         else
            local dst_path = fill(p, parsed, opts):gsub(template_dir, dst)
            fill_template(p, dst_path, parsed, opts)
         end
      end
   end

   local readme = util.path.join(opts.template_path, "README.md")
   if util.fs.exists(readme) then
      fill_template(readme, util.path.join(dst, "README.md"), parsed, opts)
   end

   if parsed.license ~= "" then
      local license = util.path.join(opts.template_path, parsed.license)
      if util.fs.exists(license) then
         fill_template(license, util.path.join(dst, "LICENSE"), parsed, opts)
      else
         print(("Warning: project.tl: license file for '%s' not found"):format(parsed.license))
      end
   end
end

function Project.new(opts)
   local self = setmetatable({}, { __index = Project })


   if not opts.proj_root then
      error("Error: project.tl: no proj_root set")
   end
   if not opts.template_path then
      error("Error: project.tl: no template_path set")
   end
   if not opts.author then opts.author = "author" end
   if not opts.repo then opts.repo = "repository" end
   if not opts.version then opts.version = "0.0.1" end
   self.opts = opts
   return self
end

function Project:create()
   local p = util.fs.readlink(self.opts.proj_root, "-f") .. "/"
   local f = Float.new(0.5, 0.3)
   f:set_lines({ p, "", "GPL3", "" })
   f:set_ext("← directory (= project name)", 0, 0, false, "character")
   f:set_ext("← language", 1, 0, false, "character")
   f:set_ext("← (license)", 2, 0, false, "float")
   f:set_ext("Press <C-Enter> to commit.", 3, 0, true, "comment")
   f:open()
   f:insert()
   f:set_cursor(0, #p)
   nvim_util.map(
   "n", "<C-CR>",
   function()
      local lines = f:get_lines()
      local opts = parse_input(lines)

      if opts.dir and opts.language then
         local d = util.fs.readlink(opts.dir, "-f")

         if util.fs.exists(d) then
            print("Error: project.tl: directory already exists")
            return
         end

         local output, ok = util.fs.mkdir(d)
         if not ok then
            print(("Error: project.tl: %s"):format(output))
            return
         end


         local name = util.path.filename(d)
         local command = self.opts.proj_cmds[opts.language]
         if command then
            local err
            output, err = command(d, name):run()
            if err then
               print(("Error: project.tl: %s"):format(output))
               return
            end
         end


         create_template(d, opts, self.opts)
         f:close()
         local after = self.opts.open_after[opts.language]
         if after then
            vim.schedule(function()
               local src_file = util.path.join(d, after)
               local exists = util.fs.exists(src_file)
               api.nvim_cmd(
               {
                  cmd = "edit",
                  args = { src_file },
                  magic = { file = false, bar = false },
               },
               {})

               if exists then
                  api.nvim_exec_autocmds("BufNewFile", { group = "teal_header" })
               end
            end)
         end
      else
         print("Error: project.tl: missing fields")
         return
      end
   end,
   { buffer = f.buf })

end

return Project
