local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local string = _tl_compat and _tl_compat.string or string







local api = vim.api
local util = require("tl-utils")

local nav = {}

function nav.goto_line()
   local indices = {}
   vim.ui.input(
   { prompt = "Goto (line;column): " },
   function(input)
      if input then
         indices = util.str.split(input, ";")
      else
         return
      end
   end)


   local row
   local col
   if #indices == 0 then
      print("\nInvalid position.")
      return
   elseif #indices == 1 then
      row = tonumber(indices[1])
      col = 0
   elseif #indices == 2 then
      row = tonumber(indices[1])
      col = tonumber(indices[2])
   end

   if row == nil then
      print("Invalid row.")
      return
   end

   if row > api.nvim_buf_line_count(0) then
      print("Specified row > line count of document.")
      return
   end

   if row and col then
      api.nvim_win_set_cursor(0, { row, col })
   end
end

function nav.buf_add()
   local file_name
   vim.ui.input(
   { prompt = "File name: " },
   function(input)
      file_name = input
   end)


   if file_name == nil then
      return
   end
   local dirname = util.path.parent(file_name)

   if not dirname then
      api.nvim_cmd({ cmd = "badd", args = { file_name } }, {})
   else
      file_name = util.path.filename(file_name)
      local cwd = vim.fn.getcwd()
      local full_path = ("%s%s"):format(cwd, dirname)

      util.fs.mkdir(full_path)
      api.nvim_cmd({ cmd = "cd", args = { full_path } }, {})
      api.nvim_cmd({ cmd = "badd", args = { file_name } }, {})
   end


   local buffers = api.nvim_list_bufs()
   for _, buf_id in ipairs(buffers) do
      local buf_name = api.nvim_buf_get_name(buf_id)

      if buf_name:match(file_name) then
         api.nvim_win_set_buf(0, buf_id)
      end
   end
end

return nav
