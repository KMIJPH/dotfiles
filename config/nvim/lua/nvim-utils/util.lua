local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then
    local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end
end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local string = _tl_compat and _tl_compat.string or string; local table =
    _tl_compat and _tl_compat.table or table







local api = vim.api
local KeymapOpts = vim.keymap.SetOptions

local M = {
    buf = {},
}


function M.syntax_match(name, pattern)
    api.nvim_cmd(
        {
            cmd = "syntax",
            args = { "match", name, pattern },
        }, {})
end

function M.keyword_match(name, pattern)
    api.nvim_cmd(
        {
            cmd = "syntax",
            args = { "keyword", name, pattern },
        }, {})
end

function M.map(mode, lhs, rhs, opts)
    if not opts then
        opts = {}
    end
    if not opts.remap then
        opts.remap = false
    end
    vim.keymap.set(mode, lhs, rhs, opts)
end

function M.comment_str()
    local cs = tostring(api.nvim_buf_get_option(0, "commentstring"))
    if not cs:find("%%s") then
        return nil
    end

    local left, right = cs:match("^(.*)%%s(.*)")
    left = left:gsub(" ", "")

    if right == "" then
        right = nil
    end

    if right then
        right = right:gsub(" ", "")
    end

    return left, right
end

function M.cursor()
    local cursor = api.nvim_win_get_cursor(0)
    local row = cursor[1]
    local col = cursor[2]
    return row, col
end

function M.visual_selection()
    local start = vim.fn.getpos("'<")
    local ending = vim.fn.getpos("'>")

    local rs = start[2]
    local cs = start[3]
    local re = ending[2]
    local ce = ending[3]
    return rs, re, cs, ce
end

function M.buf.lines()
    return api.nvim_buf_get_lines(0, 0, -1, false)
end

function M.buf.str()
    return table.concat(M.buf.lines(), "\n")
end

function M.buf.visual()
    local rs, re, cs, ce = M.visual_selection()
    local text = api.nvim_buf_get_text(0, rs - 1, cs - 1, re - 1, ce, {})
    return text
end

function M.buf.vline()
    local rs, re, _, _ = M.visual_selection()
    local lines = api.nvim_buf_get_lines(0, rs - 1, re, false)
    return lines
end

function M.buf.set_lines(rs, re, lines)
    api.nvim_buf_set_lines(0, rs - 1, re, false, lines)
end

function M.buf.set_text(rs, re, cs, ce, lines)
    api.nvim_buf_set_text(0, rs - 1, cs, re - 1, ce, lines)
end

function M.buf.insert_top(lines)
    M.buf.set_lines(1, 0, lines)
end

function M.buf.insert_cursor(lines)
    local row, _ = M.cursor()
    local indent = tonumber(vim.fn.indent(row))
    local indented = {}

    for _, line in ipairs(lines) do
        table.insert(indented, ("%s%s"):format(string.rep(" ", indent), line))
    end
    M.buf.set_lines(row + 1, row, indented)
end

return M
