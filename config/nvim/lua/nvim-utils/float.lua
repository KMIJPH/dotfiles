local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local math = _tl_compat and _tl_compat.math or math







local api = vim.api
local au = api.nvim_create_autocmd

local Float = {WinOpts = {}, AucmdPattern = {}, Size = {}, }














































local function size(w, h)
   local stats = vim.api.nvim_list_uis()
   local s = stats[1]
   local width = s.width
   local height = s.height
   return {
      width = width,
      height = height,
      win_width = math.ceil(width * w),
      win_height = math.ceil(height * h),
   }
end


local function config(s)
   return {
      relative = "editor",
      width = s.win_width,
      height = s.win_height,
      col = math.ceil((s.width - s.win_width) / 2),
      row = math.ceil((s.height - s.win_height) / 2) - 1,
      style = "minimal",
      border = "rounded",
   }
end

function Float.new(w, h)
   local self = setmetatable({}, { __index = Float })
   self.w = (w == nil and { 0.8 } or { w })[1]
   self.h = (h == nil and { 0.8 } or { h })[1]
   self.config = config(size(self.w, self.h))
   self.buf = api.nvim_create_buf(false, true)
   self.ns = api.nvim_create_namespace("")
   self.group = api.nvim_create_augroup("tl_float", { clear = true })
   self.win = nil
   return self
end

function Float:set_config(c)
   self.config = c
end

function Float:open()
   self.win = api.nvim_open_win(self.buf, true, self.config)
   au(
   { "WinResized" },
   {
      buffer = self.buf,
      callback = function()
         vim.schedule(function()
            api.nvim_win_set_config(self.win, config(size(self.w, self.h)))
         end)
      end,
      desc = "Resizes window if neovim window size changes.",
      group = self.group,
   })

   return self.win
end

function Float:insert()
   api.nvim_cmd({ cmd = "startinsert", args = {} }, {})
end

function Float:close()
   api.nvim_win_close(0, true)
end

function Float:set_ext(text, row, col, fixed, hl)
   hl = hl or "function"
   fixed = fixed or false
   row = row or 0
   col = col or 0

   local opts = {
      virt_text = { { text, hl } },
      virt_text_pos = "eol",
      virt_text_win_col = 0,
      right_gravity = false,
   }

   if not fixed then
      opts.virt_text_win_col = nil
      opts.right_gravity = true
   end

   api.nvim_buf_set_extmark(self.buf, self.ns, row, col, opts)
end

function Float:set_lines(lines)
   api.nvim_buf_set_lines(self.buf, 0, -1, false, lines)
end

function Float:get_lines()
   return api.nvim_buf_get_lines(self.buf, 0, -1, false)
end

function Float:set_cursor(row, col)
   col = col or 0
   api.nvim_win_set_cursor(self.win, { row + 1, col })
end

function Float:add_autocmd(event, cb, desc, pattern)
   desc = desc or ""

   if pattern.target == "pattern" then
      local opts = {
         callback = cb,
         desc = desc,
         group = self.group,
         pattern = pattern.pattern,
      }
      au(event, opts)
   else
      local opts = {
         callback = cb,
         desc = desc,
         group = self.group,
         buffer = pattern.pattern,
      }
      au(event, opts)
   end
end

return Float
