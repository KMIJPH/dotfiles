local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table







local api = vim.api
local Diagnostic = vim.diagnostic.Diagnostic
local ConfigOpts = vim.diagnostic.ConfigOpts
local au = api.nvim_create_autocmd
local diag = vim.diagnostic
local util = require("tl-utils")

local TLDiag = {Parsed = {}, }











local function parse_line(line)
   local row
   local col
   local msg
   row, col, msg = line:match(".?:([%d]*):([%d]*): (.+)")

   return {
      row = tonumber(row),
      col = tonumber(col),
      msg = msg,
   }
end


local function parse_diagnostics()
   local file = api.nvim_buf_get_name(0)
   local src = util.path.subdir(util.path.parent(file), "src")
   local command
   if src then
      command = util.SH.new("cd", src, "&&", "tl", "check", file):run()
   else
      command = util.SH.new("tl", "check", file):run()
   end

   local diagnostics = {}

   for _, t in ipairs({ "warning", "error", "syntax error", "type error" }) do
      local re = ("(%%d) %s[s]*:\n"):format(t)
      local severity
      if t == "warning" then
         severity = diag.severity.WARN
      else
         severity = diag.severity.ERROR
      end

      for s in command:gmatch("([^=]+)") do
         local part = util.str.trim(s)
         local n = part:match(re)

         if n then
            local lines = util.str.split_lines(part)

            for i = 2, #lines do
               if lines[i] ~= "" then
                  local d = parse_line(lines[i])
                  table.insert(
                  diagnostics,
                  {
                     lnum = d.row - 1,
                     col = d.col,
                     severity = severity,
                     message = d.msg,
                  })

               end
            end
         end
      end
   end

   return diagnostics
end

function TLDiag.new(opts)
   local self = setmetatable({}, { __index = TLDiag })
   self.id = "teal_diag"
   self.opts = opts
   return self
end


function TLDiag:set_diagnostics()
   local diagnostics = parse_diagnostics()
   local ns = api.nvim_create_namespace(self.id)
   diag.set(ns, 0, diagnostics, self.opts)
end


function TLDiag:run()
   au(
   { "BufWritePost" },
   {
      pattern = "*.tl",
      callback = function()
         self:set_diagnostics()
      end,
      desc = "Set diagnostics using 'teal check'.",
      group = api.nvim_create_augroup(self.id, { clear = true }),
   })

end

return TLDiag
