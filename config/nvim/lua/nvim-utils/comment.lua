local _tl_compat; if (tonumber((_VERSION or ''):match('[%d.]*$')) or 0) < 5.3 then local p, m = pcall(require, 'compat53.module'); if p then _tl_compat = m end end; local ipairs = _tl_compat and _tl_compat.ipairs or ipairs; local math = _tl_compat and _tl_compat.math or math; local string = _tl_compat and _tl_compat.string or string; local table = _tl_compat and _tl_compat.table or table







local api = vim.api
local util = require("tl-utils")
local nvim_util = require("nvim-utils.util")

local M = {}


local function comment(line, left, right, indent)
   local replacement
   local indent_spc = table.concat(util.arr.rep(" ", indent), "")
   local indent_tab = table.concat(util.arr.rep("\t", math.floor(indent / 4)), "")

   local space = line:match(("^%s(.+)"):format(indent_spc))
   local tab = line:match(("^%s(.+)"):format(indent_tab))

   if space then
      replacement = ("%s%s %s"):format(indent_spc, left, space)
   elseif tab then
      replacement = ("%s%s %s"):format(indent_tab, left, tab)
   else
      replacement = ("%s%s"):format(indent_spc, left)
   end

   if right then
      replacement = replacement .. " " .. right
   end

   return replacement
end


local function uncomment(line, left, right)
   local replacement
   local left_esc = util.str.regex_escape(left)
   local indent = line:match("^(%s*)" .. left_esc)

   if indent then
      if (line == left) or (line:match("^" .. indent .. left_esc .. "$") ~= nil) then
         replacement = ""
      else
         replacement = line:gsub("^" .. indent .. left_esc .. " ", indent)

         if replacement == line then
            replacement = line:gsub("^" .. indent .. left_esc, indent)
         end
      end

      if right then
         local right_esc = util.str.regex_escape(right)
         replacement = replacement:gsub(" " .. right_esc .. "$", "")
      end
   else
      replacement = line
   end

   return replacement
end


function M.comment_line()
   local row, _ = nvim_util.cursor()
   local line = api.nvim_get_current_line()
   local n_indent = vim.fn.indent(row)
   local left, right = nvim_util.comment_str()

   if not left then
      error("Error: comment.tl: could not find commentstring for current buffer")
   end

   local left_esc = util.str.regex_escape(left)
   local right_esc = ""
   if right then right_esc = util.str.regex_escape(right) end

   local replacement

   if line:match(("^[%%s]*%s[%%s]*.-%s"):format(left_esc, right_esc)) ~= nil then
      replacement = uncomment(line, left, right)
   else
      replacement = comment(line, left, right, n_indent)
   end

   nvim_util.buf.set_lines(row, row, { replacement })
end


function M.comment_selection()
   local rs, re = nvim_util.visual_selection()
   local lines = nvim_util.buf.vline()
   local left, right = nvim_util.comment_str()

   if not left then
      error("Error: comment.tl: could not find commentstring for current buffer")
   end

   local left_esc = util.str.regex_escape(left)
   local right_esc = ""
   if right then right_esc = util.str.regex_escape(right) end


   local commented = util.arr.filter(
   lines,
   function(x)
      return x:match(("^[%%s]*%s[%%s]*.-%s"):format(left_esc, right_esc)) ~= nil
   end)


   local indents = util.arr.map(
   lines,
   function(x)
      local space = x:match("^([ ]+).*")
      local tab = x:match("^([\t]+).*")
      local empty = x:match("^$")

      if space then
         return #space
      elseif tab then
         return #tab
      elseif empty then
         return nil
      else
         return 0
      end
   end)


   local min_indent = util.arr.min(indents)
   if not min_indent then min_indent = 0 end

   local replacements = {}
   for i, line in ipairs(lines) do
      if (#commented == #lines) then
         table.insert(replacements, i, uncomment(line, left, right))
      else
         table.insert(replacements, i, comment(line, left, right, min_indent))
      end
   end

   nvim_util.buf.set_lines(rs, re, replacements)
end

return M
