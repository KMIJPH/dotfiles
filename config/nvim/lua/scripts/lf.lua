#!/usr/bin/env lua
-- File Name: lf.lua
-- Description: Lf file manager settings
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 17 May 2023 13:30:54
-- Last Modified: 16 Nov 2023 19:06:57

local api  = vim.api
local util = require("tl-utils")
local LF   = require("nvim-utils.lf")

local lf   = LF.new(
    {
        temp_path = util.path.join(util.fs.temp(), "lf_selection"),
        config = util.path.join(vim.fn.stdpath("config"), "lfrc"),
        unmaps = {
            "jkl",
            "<Esc>"
        }
    }
)

local M    = {}

function M.setup()
    api.nvim_create_user_command(
        "LF",
        function()
            lf:run()
        end,
        { desc = "Opens the lf file manager in a new floating window." }
    )
end

return M
