#!/usr/bin/env lua
-- File Name: project.lua
-- Description: Project creation
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 May 2023 15:22:54
-- Last Modified: 10 Feb 2024 14:42:20

local api        = vim.api
local util       = require("tl-utils")
local Project    = require("nvim-utils.project")

local templates  = util.path.join(vim.fn.stdpath("config"), "templates", "projects")

local proj_cmds  = {
    go = function(dir, name)
        return util.SH.new("cd", dir, "&&", "go", "mod", "init", ("codeberg.org/KMIJPH/%s"):format(name))
    end,
    purescript = function(dir, _) return util.SH.new("cd", dir, "&&", "spago", "init", "-C") end,
    rust = function(dir, name) return util.SH.new("cd", dir, "&&", "cargo", "init", "--name", name) end,
}

local open_after = {
    go = "src/main.go",
    lua = "src/init.lua",
    purescript = "src/Main.purs",
    python = "src/main.py",
    rust = "src/main.rs",
    teal = "src/init.tl",
}


local p = Project.new(
    {
        proj_root = "~/Sync/scripts/",
        proj_cmds = proj_cmds,
        open_after = open_after,
        template_path = templates,
        author = "Joseph Kiem",
        repo = "__masked__",
        version = "0.0.1",
    }
)


local M = {}

function M.setup()
    api.nvim_create_user_command(
        "Project",
        function() p:create() end,
        { desc = "Create a project." }
    )
end

return M
