#!/usr/bin/env lua
-- File Name: lazygit.lua
-- Description: lazygit settings
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 17 May 2023 13:30:54
-- Last Modified: 16 Nov 2023 21:31:05

local api = vim.api
local LG  = require("nvim-utils.lazygit")

local lg  = LG.new(
    {
        unmaps = {
            "jkl",
            "<Esc>"
        }
    }
)

local M   = {}

function M.setup()
    api.nvim_create_user_command(
        "LG",
        function()
            lg:run()
        end,
        { desc = "Opens lazygit in a new floating window." }
    )
end

return M
