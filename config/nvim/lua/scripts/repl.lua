#!/usr/bin/env lua
-- File Name: repl.lua
-- Description: REPL settings
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: Wed 01 Jun 2022 04:45:21 PM CEST
-- Last Modified: 12 Sep 2024 15:04:17

local api = vim.api
local util = require("tl-utils")
local REPL = require("nvim-utils.repl")

--- commands to start a REPL for interpreted languages
local repl_commands = {
    javascript = function() return "node" end,
    julia = function() return "julia" end,
    matlab = function() return "matlab -nosoftwareopengl -nodesktop" end,
    lua = function() return "lua" end,
    nix = function() return "nix repl" end,
    purescript = function() return "spago repl" end,
    haskell = function() return "ghci" end,
    python = function()
        local root = util.fs.rev_find(vim.fn.getcwd(), "main.py")
        local command = ""
        if root then
            command = ("cd %s"):format(root)
            local ld_path =
            [[export LD_LIBRARY_PATH=$(nix-instantiate --eval -E '"${(import <nixpkgs> {}).stdenv.cc.cc.lib}/lib"' | sed s/\"//g)]]
            local venv = util.fs.rev_find(root, ".venv")
            if venv then
                command = ([[
                %s;
                %s;
                source %s/.venv/bin/activate;
                ipython --no-autoindent
                ]]
                ):format(ld_path, command, venv)
            else
                command = ([[
                %s;
                %s;
                ipython --no-autoindent
                ]]
                ):format(ld_path, command)
            end
        end
        return command
    end,
}

--- compile commands for compiled languages (or run file for interpreted)
local compile_commands = {
    go = function(_) return "go run ." end,
    groff = function(file)
        local fwe = util.path.basename(file)
        local pre_cmd = ("soelim | preconv -eutf8 %s | refer"):format(file)
        local mom_cmd = ("pdfmom -c -U -Kutf8 > %s.pdf"):format(fwe)
        api.nvim_cmd({ cmd = "!", args = { pre_cmd, mom_cmd, fwe } }, {})

        local exists = util.SH.new("pidof", "zathura"):run()
        if exists == "" then
            os.execute(("zathura %s.pdf &"):format(fwe))
        end
    end,
    html = function(file)
        os.execute(("firefox %s 2> /dev/null"):format(file))
    end,
    julia = function(file) os.execute(("julia %s"):format(file)) end,
    javascript = function(file)
        return ("cd %s && tsc && node %f"):format(util.path.subdir(file, "src"))
    end,
    lua = function(_)
        api.nvim_cmd(
            { cmd = "luafile", magic = { file = true }, args = { "%" }
            }, {}
        )
    end,
    nix = function(file)
        return ("nix repl --file %s"):format(file)
    end,
    haskell = function(file)
        return ("cd %s && cabal run"):format(util.path.subdir(file, "src"))
    end,
    purescript = function(file)
        return ("cd %s && spago run"):format(util.path.subdir(file, "src"))
    end,
    python = function(file) return ("python %s"):format(file) end,
    rust = function(_) return "cargo run" end,
    teal = function(file) return ("tl run %s"):format(file) end,
    tex = function(file)
        local fwe = util.path.basename(file)
        -- api.nvim_cmd({ cmd = "!", args = { "biber", fwe } }, {})
        api.nvim_cmd({ cmd = "!", args = { "bibtex", fwe } }, {})
        api.nvim_cmd({ cmd = "!", args = { "pdflatex", fwe } }, {})

        local exists = util.SH.new("pidof", "zathura"):run()
        if exists == "" then
            os.execute(("zathura %s.pdf &"):format(fwe))
        end
    end,
    typescript = function(file)
        return ("cd %s && tsc && node ../build/main.js"):format(util.path.subdir(file, "src"))
    end,
    typst = function(file)
        local fwe = util.path.basename(file)
        api.nvim_cmd(
            {
                cmd = "!",
                args = { "typre", "-r", "-p", "onlineImage", "-c", "typrecache", file, "|", "typst", "compile", "-", ("%s.pdf"):format(fwe) }
            },
            {}
        )

        local exists = util.SH.new("pidof", "zathura"):run()
        if exists == "" then
            os.execute(("zathura %s.pdf &"):format(fwe))
        end
    end,
}

local lua_test = function(file)
    local src = util.path.subdir(file, "src")
    local test = util.path.subdir(file, "test")
    local cmd =
    "cd %s && LUA_PATH=\"$LUA_PATH;$HOME/.luarocks/share/lua/5.2/?.lua;$HOME/.luarocks/share/lua/5.2/?/init.lua;;\" luarocks test"

    if src then
        return cmd:format(src)
    end
    if test then
        return cmd:format(test)
    end
    error("No /src or /test directory found")
end

--- test commands
local test_commands = {
    go = function(file) return ("cd %s && go test ./..."):format(util.path.subdir(file, "src")) end,
    lua = lua_test,
    purescript = function(file)
        return ("cd %s && spago test"):format(util.path.subdir(file, "src"))
    end,
    haskell = function(file)
        return ("cd %s && cabal test --test-show-details=direct --test-option=--format=progress"):format(util.path
            .subdir(file, "src"))
    end,
    python = function(_) return "python -m unittest discover" end,
    teal = lua_test,
    rust = function(_) return "cargo test -- --nocapture" end,
}

local M = {}

function M.setup()
    local repl = REPL.new(
        {
            height = 18,
            repl_cmds = repl_commands,
            test_cmds = test_commands,
            compile_cmds = compile_commands
        }
    )

    api.nvim_create_user_command(
        "REPLOpenTerm",
        function()
            repl:create()
            api.nvim_cmd({ cmd = "startinsert" }, {})
        end,
        { desc = "Opens terminal in a new split." }
    )
    api.nvim_create_user_command(
        "REPLOpenREPL",
        function() repl:toggle() end,
        { desc = "Opens/toggles REPL in a new split. If not found uses shell terminal." }
    )
    api.nvim_create_user_command(
        "REPLCloseTerm",
        function() repl:kill() end,
        { desc = "Closes/kills terminal window." }
    )
    api.nvim_create_user_command(
        "REPLSendLine",
        function() repl:send_line() end,
        { desc = "Sends current line to the REPL/terminal." }
    )
    api.nvim_create_user_command(
        "REPLCloseBuf",
        function() repl:close() end,
        { desc = "Closes buffer and switches to next non terminal buffer." }
    )
    api.nvim_create_user_command(
        "REPLSendSelection",
        function() repl:send_selection() end,
        {
            range = true, desc = "Sends current selection to the REPL/terminal."
        }
    )
    api.nvim_create_user_command(
        "REPLRunFile",
        function() repl:run_file("compile") end,
        { desc = "Runs current file in the shell." }
    )
    api.nvim_create_user_command(
        "REPLTest",
        function() repl:run_file("test") end,
        { desc = "Runs tests for current file." }
    )
    api.nvim_create_user_command(
        "REPLSwitchToTerm",
        function() repl:switch_to_term() end,
        { desc = "Switches cursor focus to the terminal window." }
    )
    api.nvim_create_user_command(
        "REPLSwitchToWin",
        function() repl:switch_to_win() end,
        { desc = "Switches focus back to the main window." }
    )
end

return M
