#!/usr/bin/env lua
-- File Name: misc.lua
-- Description: Misc 'plugins' for nvim
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 08 Oct 2022 20:33:42
-- Last Modified: 28 Jul 2024 14:34:45

local api = vim.api
local nav = require("nvim-utils.nav")

local M   = {}


function M.setup()
    api.nvim_create_user_command(
        "NvimBufAdd",
        function() nav.buf_add() end,
        { desc = "Adds buffer/file at current wd." }
    )

    api.nvim_create_user_command(
        "NvimGotoLine",
        function() nav.goto_line() end,
        { desc = "Set cursor to line/column." }
    )
end

return M
