#!/usr/bin/env lua
-- File Name: header.lua
-- Description: Header settings
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 24 May 2023 13:56:48
-- Last Modified: 16 Jul 2024 00:50:02

local api = vim.api
local util = require("tl-utils")
local Header = require("nvim-utils.header")
local header_dir = util.path.join(vim.fn.stdpath("config"), "templates", "headers")
local group = api.nvim_create_augroup("teal_header", { clear = true })


local creation_date = {
    event = "on_create",
    field = "Creation Date:",
    replace = function() return os.date("%d %b %Y %H:%M:%S") end
}
local creation_date_md = {
    event = "on_create",
    field = "date:",
    replace = function() return os.date("%Y-%m-%d %H:%M:%S") end
}
local creation_date_norg = {
    event = "on_create",
    field = "created:",
    replace = function() return os.date("%d %b %Y %H:%M:%S") end
}
local creation_date_bib = {
    event = "on_create",
    field = "urldate",
    replace = function() return ("= {%s},"):format(os.date("%Y-%m-%d")) end
}

local filename = {
    event = "on_save",
    field = "File Name:",
    replace = function() return util.path.filename(api.nvim_buf_get_name(0)) end
}
local filename_norg = {
    event = "on_save",
    field = "title:",
    replace = function() return util.path.filename(api.nvim_buf_get_name(0)) end
}

local mod_date = {
    event = "on_save",
    field = "Last Modified:",
    replace = function() return os.date("%d %b %Y %H:%M:%S") end
}
local mod_date_norg = {
    event = "on_save",
    field = "last_modified:",
    replace = function() return os.date("%d %b %Y %H:%M:%S") end
}
local mod_date_md = {
    event = "on_save",
    field = "lastmod:",
    replace = function() return os.date("%Y-%m-%d %H:%M:%S") end
}

local opts = {
    {
        extensions = "*.sh",
        prefix = { "#!/usr/bin/env bash" },
        suffix = { "", "set -eufo pipefail", "IFS=$'\\n'" },
        fields = { filename, creation_date, mod_date },
        path = util.path.join(header_dir, "header_dep.txt"),
        group = group,
        cs = { left = "#", right = nil },
    },
    {
        extensions = "*.py",
        prefix = { "#!/usr/bin/env python3" },
        suffix = {},
        fields = { filename, creation_date, mod_date },
        path = util.path.join(header_dir, "header.txt"),
        group = group,
        cs = { left = "#", right = nil },
    },
    {
        extensions = "*.lua",
        prefix = { "#!/usr/bin/env lua" },
        suffix = {},
        fields = { filename, creation_date, mod_date },
        path = util.path.join(header_dir, "header.txt"),
        group = group,
        cs = { left = "--", right = nil },
    },
    {
        extensions = { "*.purs", "*.tl" },
        prefix = {},
        suffix = {},
        fields = { filename, creation_date, mod_date },
        path = util.path.join(header_dir, "header.txt"),
        group = group,
        cs = { left = "--", right = nil },
    },
    {
        extensions = { "*.hs" },
        prefix = {},
        suffix = {},
        fields = { filename, creation_date, mod_date },
        path = util.path.join(header_dir, "header.txt"),
        group = group,
        cs = { left = "--", right = nil },
    },
    {
        extensions = { "*.go", "*.rs" },
        prefix = {},
        suffix = {},
        fields = { filename, creation_date, mod_date },
        path = util.path.join(header_dir, "header.txt"),
        group = group,
        cs = { left = "//", right = nil },
    },
    {
        extensions = "*.html",
        prefix = {},
        suffix = {},
        fields = { mod_date },
        path = util.path.join(header_dir, "header.html"),
        group = group,
        add_cs = false,
        cs = { left = nil, right = "-->" },
    },
    {
        extensions = "*.norg",
        prefix = {},
        suffix = {},
        fields = { filename_norg, creation_date_norg, mod_date_norg },
        path = util.path.join(header_dir, "header.norg"),
        group = group,
        cs = { left = nil, right = nil },
    },
    {
        extensions = "*.md",
        prefix = {},
        suffix = {},
        fields = { creation_date_md, mod_date_md },
        path = util.path.join(header_dir, "header.md"),
        group = group,
        cs = { left = nil, right = nil },
    },
    {
        extensions = "*.bib",
        prefix = {},
        suffix = {},
        fields = { creation_date_bib },
        path = util.path.join(header_dir, "header.bib"),
        group = group,
        cs = { left = nil, right = nil },
    },
    {
        extensions = { "*.conf", "*.json", "config", "*rc", "icons", "colors" },
        prefix = {},
        suffix = {},
        fields = { filename, creation_date, mod_date },
        path = util.path.join(header_dir, "header.txt"),
        group = group,
        cs = { left = nil, right = nil },
    }
}

local M = {}

function M.setup()
    for _, opt in ipairs(opts) do
        local h = Header.new(opt)
        h:on_create()
        h:on_save()
    end
    api.nvim_create_user_command(
        "HeaderAdd",
        function()
            Header.add(util.path.join(header_dir, "header.txt"), { filename, creation_date, mod_date })
        end,
        { desc = "Create a header at the top." }
    )
end

return M
