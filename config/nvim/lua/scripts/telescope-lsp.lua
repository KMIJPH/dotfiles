#!/usr/bin/env lua
-- File Name: telescope-lsp.lua
-- Description: Telescope lsp actions picker
-- Dependencies: telescope, lsp
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 08 Oct 2022 20:33:42
-- Last Modified: 16 Nov 2023 19:07:20

local api          = vim.api
local pickers      = require("telescope.pickers")
local finders      = require("telescope.finders")
local conf         = require("telescope.config").values
local actions      = require("telescope.actions")
local action_state = require("telescope.actions.state")
local lsp          = vim.lsp

local M            = {}

--- lsp actions
local lsp_actions  = {
    ["Diagnostic next"] = vim.diagnostic.goto_next,
    ["Diagnostic previous"] = vim.diagnostic.goto_prev,
    ["Code Action"] = lsp.buf.code_action,
    ["Declaration"] = lsp.buf.declaration,
    ["Definition"] = lsp.buf.definition,
    ["Docstring"] = lsp.buf.hover,
    ["Document Symbols"] = lsp.buf.document_symbol,
    ["Format"] = lsp.buf.format,
    ["References"] = lsp.buf.references,
    ["Rename"] = lsp.buf.rename,
    ["Signature Help"] = lsp.buf.signature_help,
    ["Type Definition"] = lsp.buf.type_definition,
}

--- telescope lsp picker
-- @param opts Options to pass
local function lsp_picker(opts)
    local names = {}
    for k, _ in pairs(lsp_actions) do
        table.insert(names, k)
    end

    opts = opts or {}
    pickers.new(opts, {
        prompt_title = "Lsp Actions",
        finder = finders.new_table {
            results = names
        },
        sorter = conf.generic_sorter(opts),
        attach_mappings = function(prompt_bufnr)
            actions.select_default:replace(function()
                actions.close(prompt_bufnr)
                local selection = action_state.get_selected_entry()
                local key = selection[1]
                local action = lsp_actions[key]
                action()
            end)
            return true
        end,
    }):find()
end

function M.setup()
    api.nvim_create_user_command(
        "TelescopeLsp",
        function()
            lsp_picker(require("telescope.themes").get_dropdown {})
        end,
        { desc = "Displays lsp actions using telescope." }
    )
end

return M
