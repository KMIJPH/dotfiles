#!/usr/bin/env lua
-- File Name: init.lua
-- Description: Nvim lua config
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 07 Oct 2022 16:53:34
-- Last Modified: 10 Sep 2024 21:49:06

local api       = vim.api
local au        = vim.api.nvim_create_autocmd
local util      = require("tl-utils")
local nvim_util = require("nvim-utils.util")

if not table.unpack then
    table.unpack = unpack
end

local function install_spell()
    local spelldir = util.path.join(vim.fn.stdpath("config"), "spell")
    local spellftp = "http" .. "://ftp.vim.org/vim/runtime/spell/"
    local args = {
        "--directory-prefix",
        spelldir,
        "--cut-dirs 3",
        "--no-host-directories",
        "--recursive",
        "--no-parent",
        "-R '*.html*'",
    }
    local langs = {
        "de",
        "en",
        "it",
    }
    for _, lang in pairs(langs) do
        if not util.fs.isdir(("%s/%s"):format(spelldir, lang)) then
            local output, err = util.SH.new("wget", table.unpack(args), ("%s/%s/"):format(spellftp, lang))
                :run()
            if err then
                print(output)
            end
            output, err = util.SH.new("wget", table.unpack(args), ("%s/%s.utf-8.spl"):format(spellftp, lang))
                :run()
            if err then
                print(output)
            end
            output, err = util.SH.new("wget", table.unpack(args), ("%s/%s.utf-8.sug"):format(spellftp, lang))
                :run()
            if err then
                print(output)
            end
        end
    end
end

-- downloads lazy.vim using git (__masked__)
local function install_lazy()
    local lazypath = util.path.join(vim.fn.stdpath("data"), "lazy", "lazy.nvim")
    local args = {
        "clone",
        "--filter=blob:none",
        "https" .. "://github.com/folke/lazy.nvim",
        "--branch=stable",
        lazypath,
    }

    vim.opt.rtp:prepend(lazypath)

    if not util.fs.isdir(lazypath) then
        local output, err = util.SH.new("git", table.unpack(args)):run()
        if err then
            print(output)
        end
    end
end

--- performs weekly plugin updates and updates TS languages
-- @param lazy Lazy module
local function update(lazy)
    local week_now = math.floor(os.time() / 604800)
    local file = util.path.join(vim.fn.stdpath("data"), "update_timer")

    if not util.fs.exists(file) then
        util.fs.write(file, tostring(week_now))
        week_now = week_now + 1
    end

    local content = util.fs.read(file)
    local week_prev = tonumber(content)

    if week_now > week_prev then
        api.nvim_cmd({ cmd = "TSUpdate", args = {} }, {})
        api.nvim_cmd({ cmd = "KanagawaCompile", args = {} }, {})
        lazy.update()
        util.fs.write(file, tostring(week_now))
    end
end

--- initializes nvim
-- creates dirs
-- loads spell files
-- installs lazy
local function initialize()
    -- create undo dir
    local undo_dir = util.path.join(vim.fn.stdpath("data"), "undo")
    if not util.fs.isdir(undo_dir) then
        util.fs.mkdir(undo_dir)
    end

    install_spell()
    install_lazy()

    -- initialize lazy
    local plugins = require("plugins")
    local lazy = require("lazy")
    lazy.setup(plugins)
    update(lazy)
end

--INIT--------------------------
initialize()
require("global")

--AUTOCMDs----------------------
local group = api.nvim_create_augroup("GroupGlobal", { clear = true })
au(
    "TermOpen",
    {
        pattern = "*",
        callback = function()
            nvim_util.map("t", "<Esc>", "<C-\\><C-n>", { buffer = 0 })
            nvim_util.map("t", "jkl", "<C-\\><C-n>", { buffer = 0 })
        end,
        desc = "Set terminal keybindings",
        group = group,
    }
)

au(
    { "BufNewFile", "BufRead" },
    {
        pattern = { "*.eel", "*.jsfx" },
        callback = function()
            api.nvim_cmd({ cmd = 'setf', args = { "eel2" } }, {})
            api.nvim_cmd({ cmd = "setlocal", args = { "shiftwidth=2" } }, {})
        end,
        desc = "Set terminal keybindings",
        group = group,
    }
)

--PLUGINS & SCRIPTS-------------
require("filetype")
require("scripts.header").setup()
require("scripts.misc").setup()
require("scripts.repl").setup()
require("scripts.telescope-lsp").setup()
require("scripts.lazygit").setup()
require("scripts.lf").setup()
require("scripts.project").setup()
require("mappings")
