(final: prev: {
  cyrus-sasl-xoauth2 = prev.pkgs.stdenv.mkDerivation {
    pname = "cyrus-sasl-xoauth2";
    version = "master";

    src = prev.pkgs.fetchFromGitHub {
      owner = "moriyoshi";
      repo = "cyrus-sasl-xoauth2";
      rev = "master";
      sha256 = "sha256-OlmHuME9idC0fWMzT4kY+YQ43GGch53snDq3w5v/cgk=";
    };

    nativeBuildInputs = [
      prev.pkg-config
      prev.automake
      prev.autoconf
      prev.libtool
    ];
    propagatedBuildInputs = [ prev.cyrus_sasl ];

    buildPhase = ''
      ./autogen.sh
      ./configure
    '';

    installPhase = ''
      make DESTDIR="$out" install
    '';

    meta = {
      homepage = "__masked__";
      description = "XOAUTH2 mechanism plugin for cyrus-sasl";
    };
  };

  # __masked__
  isync-oauth2 = prev.buildEnv {
    name = "isync-oauth2";
    paths = [ prev.isync ];
    pathsToLink = [ "/bin" ];
    nativeBuildInputs = [ prev.makeWrapper ];
    postBuild = ''
      wrapProgram "$out/bin/mbsync" \
        --prefix SASL_PATH : "${prev.cyrus_sasl.out.outPath}/lib/sasl2:${final.cyrus-sasl-xoauth2}/usr/lib/sasl2"
    '';
  };
})
