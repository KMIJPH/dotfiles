final: prev: {
  patchelfFixes = prev.patchelfUnstable.overrideAttrs (finalAttrs: prevAttrs: {
    src = prev.fetchFromGitHub {
      owner = "Patryk27";
      repo = "patchelf";
      rev = "527926dd9d7f1468aa12f56afe6dcc976941fedb";
      sha256 = "sha256-3I089F2kgGMidR4hntxz5CKzZh5xoiUwUsUwLFUEXqE=";
    };
  });

  pcloud-fixed = prev.pcloud.overrideAttrs (finalAttrs: prevAttrs: {
    nativeBuildInputs = prevAttrs.nativeBuildInputs ++ [ final.patchelfFixes ];
  });
}
