final: prev: {
  tex-pkgs = prev.texlive.combine {
    inherit (prev.texlive)
      scheme-basic amsmath beamer booktabs caption chemfig cleveref
      collection-fontsrecommended crossword datetime2 dvipng dvisvgm enumitem
      etoolbox extsizes fancyvrb filemod float floatrow forest hyperref
      koma-script latexindent lineno listings makecell mdwtools pdflscape
      pdfpages pgf pgfgantt pgfplots preview ragged2e regexpatch setspace
      simplekv soul standalone svg tikzmark titlesec titling transparent
      trimspaces ulem upquote wrapfig;
  };
}
