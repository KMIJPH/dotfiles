-- File Name: playlist-manager.lua
-- Description: Custom playlist manager (WIP)
-- Dependencies: mpv-user-input
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 17 Dec 2024 18:31:00
-- Last Modified: 17 Dec 2024 18:31:10

local mp            = require 'mp'
local utils         = require 'mp.utils'
package.path        = mp.command_native({ "expand-path", "~~/script-modules/?.lua;" }) .. package.path
local list          = require "scroll-list"

local pitch_map     = {}
local base_path     = "/home/jk/.config/nixos/home/desktop/mpv/etc/"
local rb_opts       = "transients=smooth" ..
    ":pitch=quality" ..
    ":window=short" ..
    ":channels=together"

local o             = {
  header = "Playlist files \\N ------------------------------------",
  wrap = true,
  reset_cursor_on_close = true,
  key_move_begin = "HOME",
  key_move_end = "END",
  key_move_pageup = "PGUP",
  key_move_pagedown = "PGDWN",
  key_scroll_down = "DOWN WHEEL_DOWN",
  key_scroll_up = "UP WHEEL_UP",
  key_loadfile = "ENTER",
  key_close_browser = "ESC MBTN_RIGHT",
}

local original_open = list.open
list.header         = o.header
list.wrap           = o.wrap

--| reads a m3u playlist file and returns a table of entries
local function read_playlist(file)
  local lines = {}
  local f = io.open(file, "r")
  if not f then
    mp.msg.error("Failed to open playlist file: " .. file)
    return nil
  end
  for line in f:lines() do
    table.insert(lines, line)
  end
  f:close()
  return lines
end

--| parses pitch information and title from the entries and queues them
local function queue_videos(lines)
  for _, line in ipairs(lines) do
    if line:match("^#") then
      mp.msg.info("Skipping commented line: " .. line)
    else
      local url, pitch, title = line:match("^(%S+)%s+([+-]?%d+%.?%d*)%s*(.*)")
      if url and pitch and title then
        mp.commandv("loadfile", url, "append", "-1", "force-media-title=" .. title)
        pitch_map[url] = tonumber(pitch)
        mp.msg.info(("Queued: %s with pitch %s"):format(url, pitch))
      elseif url then
        mp.commandv("loadfile", url, "append", "-1")
      else
        mp.msg.warn("Skipping invalid line: " .. line)
      end
    end
  end
end

--| set pitchs and filenames on file-load
local function on_load()
  mp.set_property("vid", "no")
  local path = mp.get_property("path")

  if pitch_map[path] then
    local pitch = pitch_map[path]
    local multiplier = 2 ^ (pitch / 12)
    mp.command(("af-command rb set-pitch %f"):format(multiplier))
    mp.msg.info(("Applied pitch correction: %f for %s"):format(pitch, path))
  else
    mp.msg.warn("No pitch correction found for: " .. path)
  end
end

--| main function
local function run(playlist_file)
  list:close()
  mp.set_property("vid", "no")
  mp.command(("af add '@rb:rubberband=%s'"):format(rb_opts))

  if not playlist_file then
    return
  end

  local lines = read_playlist(playlist_file)
  if not lines then
    mp.osd_message("Failed to load playlist!")
    return
  end
  queue_videos(lines)
  mp.osd_message("Playlist loaded!")
end

--| loads a playlist file
local function loadfile()
  local res = list.list[list.selected]
  if res then
    run(base_path .. res.ass)
  end
end

-- populates and opens list
function list:open()
  local files = utils.readdir(base_path, "files")

  list.list = {}
  for i, file in ipairs(files) do
    local item = {}
    item.ass = file
    list.list[i] = item
  end

  list:update()
  original_open(self)
end

list.keybinds = {}

local function add_keys(keys, name, fn, flags)
  local i = 1
  for key in keys:gmatch("%S+") do
    table.insert(list.keybinds, { key, name .. i, fn, flags })
    i = i + 1
  end
end

add_keys(o.key_scroll_down, 'scroll_down', function() list:scroll_down() end, { repeatable = true })
add_keys(o.key_scroll_up, 'scroll_up', function() list:scroll_up() end, { repeatable = true })
add_keys(o.key_move_pageup, 'move_pageup', function() list:move_pageup() end, {})
add_keys(o.key_move_pagedown, 'move_pagedown', function() list:move_pagedown() end, {})
add_keys(o.key_move_begin, 'move_begin', function() list:move_begin() end, {})
add_keys(o.key_move_end, 'move_end', function() list:move_end() end, {})
add_keys(o.key_loadfile, 'loadfile', loadfile, {})
add_keys(o.key_close_browser, 'close_browser', function() list:close() end, {})

mp.add_key_binding("Ctrl+p", "open_playlist", function() list:open() end)
mp.register_event("file-loaded", on_load)
