-- File Name: print-keys.lua
-- Description: Utility stuff
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 19 Nov 2023 02:31:09
-- Last Modified: 19 Nov 2023 11:41:08

local mp        = require 'mp'

local eq_def    = "1b=1:2b=1:3b=1:4b=1:5b=1:6b=1:7b=1:8b=1:9b=1:10b=1:11b=1:12b=1:13b=1:14b=1:15b=1:16b=1:17b=1:18b=1"
local eq_lp     = "1b=1:2b=1:3b=1:4b=1:5b=1:6b=1:7b=0:8b=0:9b=0:10b=0:11b=0:12b=0:13b=0:14b=0:15b=0:16b=0:17b=0:18b=0"
local eq_hp     = "1b=0:2b=0:3b=0:4b=0:5b=0:6b=0:7b=1:8b=1:9b=1:10b=1:11b=1:12b=1:13b=1:14b=1:15b=1:16b=1:17b=1:18b=1"

local cur_eq    = eq_def
local cur_pitch = 1.0
local cur_speed = 1.0
local cur_semi  = 0
local activated = false
local rb_opts   = "transients=smooth" ..
    ":pitch=quality" ..
    ":window=short" ..
    ":channels=together"

-- EQ stuff ------------------
local function set_eq_hp()
  if activated then
    cur_eq = eq_hp
    mp.command(("af set '@eq:superequalizer=%s'"):format(cur_eq))
    mp.osd_message("EQ highpass")
  end
end

local function set_eq_lp()
  if activated then
    cur_eq = eq_lp
    mp.command(("af set '@eq:superequalizer=%s'"):format(cur_eq))
    mp.osd_message("EQ lowpass")
  end
end

local function set_eq_flat()
  if activated then
    cur_eq = eq_def
    mp.command(("af set '@eq:superequalizer=%s'"):format(cur_eq))
    mp.osd_message("EQ flat")
  end
end

-- Pitch stuff ---------------
local function change_pitch()
  cur_pitch = 2 ^ (cur_semi / 12)
  mp.command(("af-command rb set-pitch %f"):format(cur_pitch))
  mp.osd_message(("%d semitones"):format(cur_semi))
end

local function increase_pitch()
  if activated then
    cur_semi = cur_semi + 1
    change_pitch()
  end
end

local function decrease_pitch()
  if activated then
    cur_semi = cur_semi - 1
    change_pitch()
  end
end

-- Speed stuff ---------------
local function change_speed()
  mp.command(("set speed %.2f"):format(cur_speed))
  mp.osd_message(("speed %.2f"):format(cur_speed))
end

local function increase_speed()
  if activated then
    cur_speed = cur_speed + 0.125
    change_speed()
  end
end

local function decrease_speed()
  if activated then
    cur_speed = cur_speed - 0.125
    change_speed()
  end
end

-- Switches ------------------
local function switch_on()
  mp.command(("af add '@rb:rubberband=%s'"):format(rb_opts))
  mp.command(("af add '@eq:superequalizer=%s'"):format(cur_eq))
  mp.command(("af-command rb set-pitch %f"):format(cur_pitch))
  mp.command(("set speed %f"):format(cur_speed))
  mp.osd_message(("Filters on: speed: %.2f, pitch: %d"):format(cur_speed, cur_semi))
end

local function switch_off()
  mp.command("set speed 1.0")
  mp.command("af-command rb set-pitch 1.0")
  mp.command(("af set '@eq:superequalizer=%s'"):format(eq_def))
  mp.command("af remove @eq")
  mp.command("af remove @rb")
  mp.osd_message("Filters off")
end

local function toggle_rbfilters()
  if not activated then
    switch_on()
    activated = true
  else
    switch_off()
    activated = false
  end
end

-- OTHERS ------------------

--| prints custom script keys
local function print_keys()
  local s = [[
    filter: increase pitch      }
    filter: decrease pitch      }
    filter: increase speed      [
    filter: decrease speed      ]
    filter: toggle              \
    filter: highpass            %
    filter: lowpass             $
    filter: flat                §
    playlist: open              Ctrl+p
    playlist: down 10           Ctrl+d
    playlist: up 10             Ctrl+u
    playlist: begin             HOME
    playlist: end               END
    visualizer: cycle           c
    yank: yank                  y
    ]]
  mp.osd_message(s)
end

--| copies current media title to the clipboard
local function get_title()
  local val = mp.get_property("media-title", "string")
  local session_type = os.getenv("XDG_SESSION_TYPE")

  if session_type == "wayland" then
    os.execute(("wl-copy %q"):format(val))
  else
    os.execute(("printf '%%s' '%q' | xsel -bi"):format(val))
  end

  mp.osd_message(("copied: %s"):format(val))
end

mp.add_key_binding("y", "get_title", get_title)
mp.add_key_binding("?", "print_keys", print_keys)
mp.add_key_binding("}", 'increase_pitch', increase_pitch)
mp.add_key_binding("{", 'decrease_pitch', decrease_pitch)
mp.add_key_binding("]", 'increase_speed', increase_speed)
mp.add_key_binding("[", 'decrease_speed', decrease_speed)
mp.add_key_binding("\\", 'toggle_rbfilters', toggle_rbfilters)
mp.add_key_binding("%", 'eq_hp', set_eq_hp)
mp.add_key_binding("$", 'eq_lp', set_eq_lp)
mp.add_key_binding("§", 'eq_flat', set_eq_flat)
