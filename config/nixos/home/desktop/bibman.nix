with import ../common.nix;

{
  home-manager.users.${user} = {
    xdg.configFile."bibman/bibman.conf".text = ''
      resourceFormats = ["pdf", "epub"]  # file extensions to recognize as resources
      bibPath = "$(HOME)/.config/nixos/home/desktop/bib"  # bibliography directory
      dlProvider = "__masked__"  # download provider
      filters = ["fixDate"]  # default filters to use

      citekey {
          order = ["author", "year", "title"]  # one or more of ["author", "year", "title"]
          lengths = [20, 4, 20]  # maximum character length for each item in 'order'
          sep = "-"
      }

      tui {
          columns = ["author", "year", "title"]  # one or more of ["author", "year", "title", "publisher", "container"]
          columnWidths = [15, 4, 80]  # width of each column (making up 99%)
          clipboardCmd = "wl-copy %s"
          open {
              cmd = "zathura %s &"
          }
          doiUrl {
              cmd = "firefox %s &"
          }
          edit {
              cmd = "nvim %s"
              terminal = true  # runs the application in the terminal by piping or inheriting data streams
          }
      }
    '';
  };
}
