with import ../common.nix;

{
  home-manager.users.${user} = {
    home.file.".local/bin/teams-lasis" = {
      text = ''
        teams-for-linux --customUserDir="/home/${user}/.local/share/teams-data/lasis"'';
      executable = true;
    };
    home.file.".local/bin/teams-snets" = {
      text = ''
        teams-for-linux --customUserDir="/home/${user}/.local/share/teams-data/snets"'';
      executable = true;
    };
  };
}
