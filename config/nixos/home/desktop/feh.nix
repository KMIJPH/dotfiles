with import ../common.nix; {
  home-manager.users."${user}" = {
    programs.feh = {
      enable = true;
      buttons = {
        prev_img = null;
        next_img = null;
        zoom = null;
        zoom_in = 4;
        zoom_out = 5;
      };
      keybindings = {
        reload_image = null;
        save_image = null;
        size_to_image = "s";
        orient_1 = "r";
      };
    };
    xdg.desktopEntries = {
      feh = {
        name = "Feh";
        genericName = "Image viewer";
        comment = "Image viewer and cataloguer";
        exec = "feh -Tdefault --start-at %U";
        terminal = false;
        icon = "feh";
        categories = [ "Graphics" "2DGraphics" "Viewer" ];
        mimeType = [
          "image/bmp"
          "image/gif"
          "image/jpeg"
          "image/jpg"
          "image/pjpeg"
          "image/png"
          "image/tiff"
          "image/webp"
          "image/x-bmp"
          "image/x-pcx"
          "image/x-png"
          "image/x-portable-anymap"
          "image/x-portable-bitmap"
          "image/x-portable-graymap"
          "image/x-portable-pixmap"
          "image/x-tga"
          "image/x-xbitmap"
          "image/heic"
        ];
      };
    };

    xdg.configFile."feh/themes".text = ''
      default --scale-down --force-aliasing
    '';
  };
}
