with import ../common.nix; {
  home-manager.users.${user} = {
    programs.zathura = {
      enable = true;
      options = {
        selection-clipboard = "clipboard";
        guioptions = "none";
      };
      mappings = {
        q = "quit";
        f = "toggle_fullscreen";
        "[fullscreen] f" = "toggle_fullscreen";
        "<Space>" = "";
        o = "display_link";
        O = "follow";
        i = "toggle_statusbar";
        H = ''exec "/home/${user}/.config/zathura/open '$FILE' '$PAGE'"'';
      };
    };
    xdg.configFile."zathura/open" = {
      text = ''firefox "file://$(realpath "$1")#page=$2"'';
      executable = true;
    };
    xdg.desktopEntries = {
      zathura = {
        name = "Zathura";
        comment = "A minimalistic document viewer";
        exec = "zathura %U";
        terminal = false;
        icon = "org.pwmt.zathura";
        categories = [
          "Office"
          "Viewer"
        ];
      };
    };
  };
}
