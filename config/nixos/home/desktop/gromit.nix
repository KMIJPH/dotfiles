with import ../common.nix;

{
  home-manager.users.${user} = {
    xdg.configFile."gromit-mpx.cfg".text = ''
      # Default gromit-mpx configuration 
      "red Pen" = PEN (size=5 color="red");
      # "red Line" = LINE (color="red");
      # "red Rect" = RECT (color="red");
      "red Arrow" = "red Pen" (arrowsize=2);

      "Eraser" = ERASER (size = 75);

      "default" = "red Pen";
      # "default"[SHIFT] = "red Line";
      # "default"[CONTROL] = "red Rect";
      "default"[2] = "red Arrow";
      "default"[Button3] = "Eraser";
    '';
  };
}
