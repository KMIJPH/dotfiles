#!/usr/bin/env bash
# File Name: bibman_bash_completion.sh
# Description: Completion file for bash (heavily inspired by pass completion)
# Author: KMIJPH
# Repository: codeberg.org/KMIJPH
# License: GPL3
# Creation Date: 20 Jan 2024 10:13:43
# Last Modified: 21 Jan 2024 23:31:45

_bibman_complete_folders() {
    local prefix="${BIB_PATH}"
    prefix="${prefix%/}/"

    compopt -o nospace

    local IFS=$'\n'
    local items=($(compgen -d $prefix$cur))
    for item in "${items[@]}"; do
        [[ $item == $prefix.* ]] && continue
        COMPREPLY+=("${item#"$prefix"}/")
    done
}

_bibman() {
    COMPREPLY=()
    local cur="${COMP_WORDS[COMP_CWORD]}"
    local commands="add file export tui tree list citekey ${BIBMAN_EXTENSION_COMMANDS[*]}"
    if [[ $COMP_CWORD -gt 1 ]]; then
        case "${COMP_WORDS[1]}" in
        add)
            COMPREPLY+=($(compgen -W "-d --dl -n --no-edit-retry" -- ${cur}))
            _bibman_complete_folders
            ;;
        file)
            COMPREPLY+=($(compgen -W "-d --dl -n --no-edit-retry" -- ${cur}))
            if [[ "$COMP_CWORD" -eq 2 ]]; then
                COMPREPLY+=($(compgen -f -- ${cur}))
            else
                _bibman_complete_folders
            fi
            ;;
        export | tui | tree)
            _bibman_complete_folders
            ;;
        esac

        if [[ " ${BIBMAN_EXTENSION_COMMANDS[*]} " == *" ${COMP_WORDS[1]} "* ]] && type "__bibman_extension_complete_${COMP_WORDS[1]}" &>/dev/null; then
            "__bibman_extension_complete_${COMP_WORDS[1]}"
        fi
    else
        COMPREPLY+=($(compgen -W "${commands}" -- ${cur}))
    fi
}

complete -o filenames -F _bibman bibman

