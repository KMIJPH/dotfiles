# dh (delete highlight)
# t (table of contents)
# m (mark)
# b (bookmark)
# db (delete bookmark)
# gb (goto bookmark)
# gh/H (goto highlight)

with import ../common.nix; {
  home-manager.users.${user} = {
    # xdg.desktopEntries = {
    #   sioyek = {
    #     name = "Sioyek";
    #     comment = "PDF viewer for reading research papers and technical books";
    #     exec = "appimage-run /home/jk/.local/bin/Sioyek-x86_64.AppImage %F";
    #     terminal = false;
    #     icon = "sioyek-icon-linux";
    #     categories = [ "Development" "Viewer" "Office" ];
    #     mimeType = [ "application/pdf" ];
    #   };
    # };

    programs.sioyek = {
      enable = true;
      bindings = {
        "move_right" = "l";
        "move_left" = "h";
        "goto_left" = "<left>";
        "goto_right" = "<right>";
        "goto_top_of_page" = "<up>";
        "goto_bottom_of_page" = "<down>";
        "next_page;goto_top_of_page" = "J";
        "previous_page;goto_top_of_page" = "K";
        "goto_beginning;goto_top_of_page" = "gg";
        "goto_end;goto_bottom_of_page" = "G";
        "fit_to_page_height" = "s";
        "fit_to_page_width" = "S";
        "add_highlight" = "H";
        "regex_search" = "/";
        "external_search" = "#";
        "goto_page_with_page_number" = "gt";
        "goto_mark" = "gm";
        "debug" = "q"; # unmap q
      };
      config = {
        "papers_folder_path" = "/home/${user}/.config/nixos/home/desktop/bib";
        "shared_database_path" =
          "/home/${user}/.config/nixos/home/desktop/sioyek/shared.db";
        "middle_click_search_engine" = "g";
        "should_highlight_unselected_search" = "1";
        "search_url_g" = "__masked__=";
        "search_url_s" = "__masked__=";
        "text_highlight_color" = "0.5 0.7 0.8";
        "search_highlight_color" = "0.5 0.7 0.8";
        "link_highlight_color" = "0.1 0.5 1.0";
        "highlight_color_a" = "1.0 1.0 0.3";
        "highlight_color_b" = "0.0 0.8 1.0";
        "highlight_color_g" = "0.3 1.0 0.3";
        "highlight_color_o" = "1.0 0.8 0.3";
        "highlight_color_p" = "0.8 0.5 1.0";
        "highlight_color_r" = "1.0 0.3 0.3";
        "highlight_color_y" = "1.0 1.0 0.3";
        "horizontal_move_amount" = "-1";
        "default_dark_mode" = "0";
        "should_launch_new_window" = "1";
        "flat_toc" = "0";
        "collapsed_toc" = "0";
        "super_fast_search" = "1";
        # "startup_commands" = "toggle_scrollbar";
      };
    };
  };
}
