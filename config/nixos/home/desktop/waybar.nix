{ pkgs, ... }:

with import ../common.nix;

let
  modules = {
    "hyprland/workspaces" = {
      format = "{icon}";
      on-click = "activate";
    };
    backlight = {
      format = "{icon} {percent}%";
      interval = 2;
      on-click-right = "light -S 1";
      on-scroll-up = "light -A 7";
      on-scroll-down = "light -U 7";
      format-icons = [ "" ];
    };
    battery = {
      format = "{icon} {capacity}%";
      format-alt = "{icon} {time}";
      format-time = "{H}:{M}";
      interval = 60;
      format-icons = [ "󰁻" "󰁽" "󰁿" "󰂁" "󰁹" ];
      tooltip = true;
      states = {
        warning = 30;
        critical = 15;
      };
    };
    clock = {
      format = "{:%H:%M}";
      format-alt = "{:%a, %d. %b  %H:%M}";
      interval = 60;
    };
    cpu = {
      format = "{icon}{usage:3d}%";
      interval = 10;
      tooltip = true;
      on-click = "bash -c '${apps.terminal} -e btm'";
      format-icons = [ "" ];
      states = {
        warning = 50;
        critical = 80;
      };
    };
    memory = {
      format = "{icon}{percentage:3d}%";
      interval = 10;
      tootip = true;
      tooltip-format = "{used:0.1f}GB used";
      on-click = "bash -c '${apps.terminal} -e btm'";
      format-icons = [ "☰" ];
      states = {
        warning = 50;
        critical = 80;
      };
    };
    keyboard-state = {
      numlock = false;
      capslock = true;
      format = "{icon}";
      format-icons = {
        locked = "󰌎";
        unlocked = "";
      };
    };
    pulseaudio = {
      format = "{icon} {volume}%";
      format-muted = "󰸈";
      format-bluetooth = " {volume}% {format_source}";
      format-bluetooth-muted = " {volume}% {format_source}";
      format-source = "{volume}%";
      format-source-muted = "";
      format-icons = {
        headphone = "󰋋";
        hands-free = "󰋋";
        headset = "󰋋";
        phone = "";
        portable = "󰋋";
        car = "";
        default = [ "󱄠" ];
      };
      on-click = "pavucontrol";
      on-click-right = "pactl set-sink-mute @DEFAULT_SINK@ toggle";
      states = {
        normal = 100;
        warning = 500;
      };
    };
    tray = { spacing = 10; };
    "custom/disk_data" = {
      interval = 300;
      format = "{icon} {}%";
      format-icons = [ "󰗮" ];
      return-type = "json";
      exec = "bar-disk /mnt/Data";
      on-click = "${apps.terminal} -e lf /mnt/Data";
      tooltip = true;
    };
    "custom/disk_root" = {
      interval = 300;
      format = "{icon} {}%";
      format-icons = [ "󰗮" ];
      return-type = "json";
      exec = "bar-disk /";
      on-click = "${apps.terminal} -e lf /";
      tooltip = true;
    };
    "custom/disk_cloud" = {
      interval = 300;
      format = "{icon} {}%";
      format-icons = [ "" ];
      return-type = "json";
      exec = "mail-disk ~/pCloudDrive";
      on-click = "${apps.terminal} -e lf ~/pCloudDrive";
      tooltip = true;
    };
    "custom/mail" = {
      interval = 900;
      format = "{icon} {}";
      format-icons = [ "󰇮" ];
      return-type = "json";
      exec-if = "bar-mail exec-if";
      exec = "bar-mail count";
      on-click = "bar-mail open";
      tooltip = true;
    };
    "custom/usb" = {
      interval = 10;
      format = "{icon} {}";
      format-icons = [ "󰕓" ];
      return-type = "json";
      exec-if = "bar-usb exec-if";
      exec = "bar-usb count";
      on-click = "bar-usb unmount";
      tooltip = true;
    };
    "custom/weather" = {
      interval = 3600;
      format = "{}";
      return-type = "json";
      exec = "wettr waybar";
      tooltip = true;
    };
  };

in {
  home-manager.users.${user} = {
    programs.waybar = {
      enable = true;
      package = pkgs.waybar;
      settings = [
        ({
          layer = "top";
          output = [ "DP-1" ];
          spacing = 4;
          height = 30;
          modules-left = [ "hyprland/workspaces" ];
          modules-center = [ "clock" ];
          modules-right = [
            "custom/mail"
            "custom/disk_root"
            "custom/disk_data"
            "custom/weather"
            "custom/usb"
            "cpu"
            "memory"
            "pulseaudio"
            "tray"
          ];
        } // modules)
        ({
          layer = "top";
          output = [ "eDP-1" ];
          spacing = 4;
          height = 30;
          modules-left = [ "hyprland/workspaces" ];
          modules-center = [ "clock" ];
          modules-right = [
            "custom/mail"
            "custom/disk_root"
            "custom/weather"
            "custom/usb"
            "cpu"
            "memory"
            "pulseaudio"
            "backlight"
            "battery"
            "keyboard-state"
            "tray"
          ];
        } // modules)
        ({
          layer = "top";
          output = [ "DP-2" ];
          spacing = 4;
          height = 30;
          modules-left = [ "hyprland/workspaces" ];
          modules-center = [ "clock" ];
          modules-right = [ ];
        } // modules)
      ];
      style = ''
        * {
            border:              none;
            font-family:         "${fonts.mono.name}", Regular;
            font-weight:         normal;
            font-size:           15px;
            box-shadow:          none;
            text-shadow:         none;
            transition-duration: 0s;
        }
        window {
            background: transparent;
            color: ${colors.fg};
        }
        #workspaces {
            margin: 0 5px;
        }
        #workspaces button:hover {
            background: ${colors.blue3};
        }
        #workspaces button {
            color: ${colors.grey2};
            padding: 0 5px;
        }
        #workspaces button.occupied {
            color: ${colors.fg};
        }
        #workspaces button.active {
            background-color: ${colors.fg};
            color: ${colors.bg};
        }
        #workspaces button.urgent {
            background-color: ${colors.orange};
        }
        #tray {
            padding: 5px;
        }
        #clock {
            padding: 5px;
        }
        #keyboard-state label.locked {
            border-radius: 12px;
            background-color: ${colors.red};
            padding: 5px;
        }
        #pulseaudio, #battery, #backlight, #memory, #cpu, #custom-usb {
            border-radius: 12px;
            background-color: alpha(${colors.blue0}, 0.6);
            padding: 5px;
        }
        #custom-disk_root, #custom-disk_data, #custom-disk-cloud,
        #custom-mail, #custom-weather {
            border-radius: 12px;
            padding: 5px;
        }
        *.warning {
            border-left: solid 5px ${colors.yellow};
            color: ${colors.fg};
        }
        *.critical {
            border-left: solid 5px ${colors.red};
            color: ${colors.fg};
        }
        *.alert {
            background-color: ${colors.orange};
            color: ${colors.bg};
        }
        *.charging {
            border-left: solid 5px ${colors.green0};
        }
        *.muted {
            border-left: solid 5px ${colors.red};
        }
      '';
    };
  };
}
