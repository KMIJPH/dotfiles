with import ../common.nix;
{
  home-manager.users.${user} = {
    programs.mpv = {
      enable = true;
      config = {
        fs = false;
        keep-open = true;
        ontop = false;
        profile = "gpu-hq";
        osd-font-size = 24;
        osd-font = fonts.mono.name;
        osd-duration = 3000;
        hwdec = "auto";
        audio-pitch-correction = true;
        slang = "en";
        alang = "en,de";
      };
      bindings = {
        "Alt++" = "add video-zoom +0.25";
        "Alt+-" = "add video-zoom -0.25";
        "Alt+LEFT" = "add video-pan-x +.05";
        "Alt+RIGHT" = "add video-pan-x -.05";
        "Alt+UP" = "add video-pan-y +.05";
        "Alt+DOWN" = "add video-pan-y -.05";
        BS = "set time-pos 0";
        q = "quit";
        Q = "quit-watch-later";
        s = "cycle sub-visibility";
        "." = "frame-step";
        "," = "frame-back-step";
        n = "playlist-next";
        p = "playlist-prev";
        r = "playlist-shuffle";
        l = "show-text '\${playlist}'";
        L = "cycle-values loop-playlist 'inf' 'no'";
        WHEEL_UP = "add volume 5";
        WHEEL_DOWN = "add volume -5";
        SPACE = "cycle pause";
        UP = "add volume 5";
        DOWN = "add volume -5";
      };
      scriptOpts = {
        playlistmanager = {
          resolve_url_titles = true;
        };
      };
    };

    xdg.desktopEntries = {
      # opens mpv socket
      mpv = {
        name = "mpv Media Player";
        genericName = "Multimedia player";
        comment = "Play movies and songs";
        exec = "mpv --player-operation-mode=pseudo-gui --input-ipc-server=/tmp/mpvsocket -- %U";
        terminal = false;
        icon = "mpv";
        categories = [
          "AudioVideo"
          "Audio"
          "Video"
          "Player"
          "TV"
        ];
        mimeType = [
          "application/ogg"
          "application/x-ogg"
          "application/mxf"
          "application/sdp"
          "application/smil"
          "application/x-smil"
          "application/streamingmedia"
          "application/x-streamingmedia"
          "application/vnd.rn-realmedia"
          "application/vnd.rn-realmedia-vbr"
          "audio/aac"
          "audio/x-aac"
          "audio/vnd.dolby.heaac.1"
          "audio/vnd.dolby.heaac.2"
          "audio/aiff"
          "audio/x-aiff"
          "audio/m4a"
          "audio/x-m4a"
          "application/x-extension-m4a"
          "audio/mp1"
          "audio/x-mp1"
          "audio/mp2"
          "audio/x-mp2"
          "audio/mp3"
          "audio/x-mp3"
          "audio/mpeg"
          "audio/mpeg2"
          "audio/mpeg3"
          "audio/mpegurl"
          "audio/x-mpegurl"
          "audio/mpg"
          "audio/x-mpg"
          "audio/rn-mpeg"
          "audio/musepack"
          "audio/x-musepack"
          "audio/ogg"
          "audio/scpls"
          "audio/x-scpls"
          "audio/vnd.rn-realaudio"
          "audio/wav"
          "audio/x-pn-wav"
          "audio/x-pn-windows-pcm"
          "audio/x-realaudio"
          "audio/x-pn-realaudio"
          "audio/x-ms-wma"
          "audio/x-pls"
          "audio/x-wav"
          "video/mpeg"
          "video/x-mpeg2"
          "video/x-mpeg3"
          "video/mp4v-es"
          "video/x-m4v"
          "video/mp4"
          "application/x-extension-mp4"
          "video/divx"
          "video/vnd.divx"
          "video/msvideo"
          "video/x-msvideo"
          "video/ogg"
          "video/quicktime"
          "video/vnd.rn-realvideo"
          "video/x-ms-afs"
          "video/x-ms-asf"
          "audio/x-ms-asf"
          "application/vnd.ms-asf"
          "video/x-ms-wmv"
          "video/x-ms-wmx"
          "video/x-ms-wvxvideo"
          "video/x-avi"
          "video/avi"
          "video/x-flic"
          "video/fli"
          "video/x-flc"
          "video/flv"
          "video/x-flv"
          "video/x-theora"
          "video/x-theora+ogg"
          "video/x-matroska"
          "video/mkv"
          "audio/x-matroska"
          "application/x-matroska"
          "video/webm"
          "audio/webm"
          "audio/vorbis"
          "audio/x-vorbis"
          "audio/x-vorbis+ogg"
          "video/x-ogm"
          "video/x-ogm+ogg"
          "application/x-ogm"
          "application/x-ogm-audio"
          "application/x-ogm-video"
          "application/x-shorten"
          "audio/x-shorten"
          "audio/x-ape"
          "audio/x-wavpack"
          "audio/x-tta"
          "audio/AMR"
          "audio/ac3"
          "audio/eac3"
          "audio/amr-wb"
          "video/mp2t"
          "audio/flac"
          "audio/mp4"
          "application/x-mpegurl"
          "video/vnd.mpegurl"
          "application/vnd.apple.mpegurl"
          "audio/x-pn-au"
          "video/3gp"
          "video/3gpp"
          "video/3gpp2"
          "audio/3gpp"
          "audio/3gpp2"
          "video/dv"
          "audio/dv"
          "audio/opus"
          "audio/vnd.dts"
          "audio/vnd.dts.hd"
          "audio/x-adpcm"
          "application/x-cue"
          "audio/m3u"
        ];
      };
    };

    home.file.".local/bin/mpv-gui" = {
      text = "mpv --player-operation-mode=pseudo-gui";
      executable = true;
    };

    # custom scripts
    xdg.configFile."mpv/scripts/utility.lua".source = ./mpv/scripts/utility.lua;
    xdg.configFile."mpv/scripts/playlist-pitch.lua".source = ./mpv/scripts/playlist-pitch.lua;

    # dependencies
    xdg.configFile."mpv/scripts/visualizer.lua".source =
      let
        repo = builtins.fetchGit {
          url = "__masked__";
          rev = "bff344ee2aeaa0153c7e593dc262d68bcc3031c6";
        };
      in
      "${repo}/visualizer.lua";

    xdg.configFile."mpv/scripts/playlist-manager.lua".source =
      let
        repo = builtins.fetchGit {
          url = "__masked__";
          rev = "c5526169ada174b0a45be55ee020a52126b54cc5";
        };
      in
      "${repo}/playlistmanager.lua";

    xdg.configFile."mpv/scripts/file-browser".source =
      let
        repo = builtins.fetchGit {
          url = "__masked__";
          rev = "462e8c0e2735f197d51d45243b931d8b0f812926";
        };
      in
      "${repo}";

    xdg.configFile."mpv/script-modules/scroll-list.lua".source =
      let
        repo = builtins.fetchGit {
          url = "__masked__";
          rev = "b8a3498b35fbb1d1d08aff98079acdb0da672277";
        };
      in
      "${repo}/scroll-list.lua";
  };
}
