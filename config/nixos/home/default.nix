{ pkgs, lib, ... }:

with import ./common.nix;
{
  imports = [
    ./terminal/bash.nix
    ./terminal/kitty.nix
    ./terminal/lf.nix
    ./terminal/lazygit.nix
    ./terminal/bottom.nix
    ./mail/mbsync.nix
    ./mail/aerc.nix
    ./desktop/hypr.nix
    ./desktop/waybar.nix
    ./desktop/feh.nix
    ./desktop/sioyek.nix
    ./desktop/vpn.nix
    ./desktop/zathura.nix
    ./desktop/gromit.nix
    ./desktop/mpv.nix
    ./desktop/bibman.nix
    ./desktop/teams.nix
  ];

  home-manager.useGlobalPkgs = true;
  # journalctl -xe --unit home-manager-jk.service
  home-manager.backupFileExtension = "backup";

  home-manager.users."${user}" = {
    home.pointerCursor = {
      x11.enable = true;
      gtk.enable = true;
      name = "volantes_cursors";
      package = pkgs.volantes-cursors;
      size = 24;
    };

    home.file.".icons/default/index.theme".text = ''
      [Icon Theme]
      Name=Default
      Comment=Default Cursor Theme
      Inherits=volantes_cursors
    '';

    gtk = {
      enable = true;
      font = {
        name = "${fonts.serif.name} Sans";
        size = 12;
        package = pkgs.${fonts.serif.package};
      };
      theme = {
        name = "Nordic";
        package = pkgs.nordic;
      };
      iconTheme = {
        name = "Papirus-Dark";
        package = pkgs.papirus-icon-theme.override { color = "nordic"; };
      };
      cursorTheme = {
        name = "Volantes Cursors 24";
        package = pkgs.volantes-cursors;
      };
      gtk3 = {
        bookmarks = [
          "file:///mnt/NAS"
          "file:///mnt/Data"
          "file:///mnt/Music"
          "file:///home/${user}/Downloads"
          "file:///home/${user}/Sync/scripts"
          "file:///home/${user}/.config/nixos/home/desktop/bib"
        ];
      };
    };

    qt = {
      enable = true;
      platformTheme.name = "gtk";
      style = {
        name = "adwaita-dark";
        package = pkgs.adwaita-qt;
      };
    };

    programs.git = {
      enable = true;
      userName = "Joseph Kiem";
      userEmail = "__masked__";
      extraConfig = {
        commit.gpgsign = true;
        user.signingkey = "A772FEA40435EA3CCCFD95C86556B334CEF8F179";
        init.defaultBranch = "main";
      };
    };

    # iana.org/assignments/media-types/media-types.xhtml
    # file -i <file>
    # xdg-mime query filetype <file>
    # xdg-mime query default <file>
    xdg.mimeApps = {
      enable = true;
      defaultApplications = lib.zipAttrsWith (_: values: values) (
        let
          subtypes =
            type: program: subt:
            builtins.listToAttrs (
              builtins.map (x: {
                name = type + "/" + x;
                value = program;
              }) subt
            );
        in
        [
          {
            "application/pdf" = "${apps.pdf}.desktop";
            "text/html" = "${apps.browser}.desktop";
            "inode/directory" = "${apps.filebrowser_gui}.desktop";
            "x-scheme-handler/mailto" = "${apps.mailer}.desktop";
          }
          (subtypes "audio" "${apps.player}.desktop" [
            "aac"
            "mp4"
            "mpeg"
            "ogg"
            "opus"
            "vorbis"
            "x-wav"
          ])
          (subtypes "video" "${apps.player}.desktop" [
            "H261"
            "H264"
            "matroska"
            "mp4"
            "mpeg"
          ])
          (subtypes "application" "${apps.editor}.desktop" [
            "json"
            "toml"
            "xml"
            "yaml"
          ])
          (subtypes "audio" "${apps.editor}.desktop" [ "x-mod" ])
          (subtypes "text" "${apps.editor}.desktop" [
            "css"
            "csv"
            "javascript"
            "julia"
            "markdown"
            "plain"
            "richtext"
            "rust"
            "vcard"
            "vnd.trolltech.linguist"
            "x-bibtex"
            "x-go"
            "x-haskell"
            "x-objsrc"
            "x-python"
            "x-typst"
            "xml"
          ])
          (subtypes "image" "${apps.images}.desktop" [
            "bmp"
            "gif"
            "jpeg"
            "png"
            "svg"
            "svg+xml"
            "tiff"
            "webp"
          ])
          (subtypes "application" "${apps.writer}.desktop" [
            "msword" # .doc
            "vnd.oasis.opendocument.spreadsheet" # .ods
            "vnd.oasis.opendocument.text" # .odt
            "vnd.openxmlformats-officedocument.wordprocessingml.document" # .docx
          ])
          (subtypes "application" "${apps.powerpoint}.desktop" [
            "vnd.ms-powerpoint" # .ppt
            "vnd.ms-powerpoint.slideshow.macroEnabled.12" # .ppsx
            "vnd.oasis.opendocument.presentation" # .odp
            "vnd.openxmlformats-officedocument.presentationml.presentation" # .pptx
          ])
          (subtypes "application" "${apps.spreadsheet}.desktop" [
            "vnd.ms-excel" # .xls
            "vnd.ms-excel.sheet.macroEnabled.12" # .xlsm
            "vnd.openxmlformats-officedocument.spreadsheetml.sheet" # .xlsx
          ])
        ]
      );
    };

    xdg.desktopEntries = {
      # opens hx in a terminal
      hx = {
        name = "Helix";
        genericName = "Text Editor";
        comment = "Edit text files";
        exec = "${apps.terminal} -e hx %F";
        terminal = false;
        icon = "hx";
        categories = [
          "Utility"
          "TextEditor"
        ];
        mimeType = [
          "text/english"
          "text/plain"
          "text/x-makefile"
          "text/x-c++hdr"
          "text/x-c++src"
          "text/x-chdr"
          "text/x-csrc"
          "text/x-java"
          "text/x-moc"
          "text/x-pascal"
          "text/x-tcl"
          "text/x-tex"
          "application/x-shellscript"
          "text/x-c"
          "text/x-c++"
        ];
      };
    };

    home.file.".gnupg/gpg-agent.conf".text = ''
      default-cache-ttl 15780000
      max-cache-ttl 15780000
      pinentry-program /run/current-system/sw/bin/pinentry-gtk-2
    '';

    xdg.configFile."pycodestyle".text = ''
      [pycodestyle]
      max-line-length = 125
      ignore = E221, E266, W391, C901, I001
    '';

    # overwrite mimeinfo.cache
    xdg.dataFile."applications/mimeinfo.cache".source = /home/${user}/.local/share/applications/mimeapps.list;

    xdg.configFile."mypy/config".text = ''
      [mypy]
      disallow_untyped_defs = True
      disallow_any_unimported = True
      no_implicit_optional = True
      check_untyped_defs = True
      warn_return_any = False
      show_error_codes = True
    '';

    # create directories
    home.file."Sync/scripts/.keep".text = "";
    home.file."Sync/configs/.keep".text = "";

    home.stateVersion = version;
  };
}
