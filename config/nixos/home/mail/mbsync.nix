{ pkgs, ... }:
with import ../common.nix;

# __masked__/

let
  normal = {
    enable = true;
    create = "maildir";
    remove = "both";
    expunge = "both";
    extraConfig = {
      account = {
        PipelineDepth = 51;
        Timeout = 120;
        SSLVersions = "TLSv1.2";
      };
    };
  };

  oauth2 = {
    enable = true;
    create = "maildir";
    remove = "both";
    expunge = "both";
    extraConfig = {
      account = {
        PipelineDepth = 50;
        Timeout = 120;
        TLSType = "IMAPS";
        SSLVersions = "TLSv1.2";
        AuthMechs = "XOAUTH2";
      };
    };
  };

in
{
  home-manager.users.${user} = {
    programs.mbsync = {
      enable = true;
      package = pkgs.isync-oauth2;
    };

    accounts.email = {
      accounts.outlook = {
        primary = true;
        realName = "Joseph Kiem";
        address = "__masked__";
        userName = "__masked__";
        imap = {
          host = "outlook.office365.com";
          port = 993;
        };
        mbsync = oauth2;
        passwordCommand = ''poauth2 refresh /home/jk/.config/aerc/oauth2/ms-work.json "pass show mail/lasis_oauth2" "cat /tmp/ms" "tee /tmp/ms"'';
      };
      accounts.lasis = {
        realName = "Joseph Kiem";
        address = "__masked__";
        userName = "__masked__";
        imap = {
          host = "outlook.office365.com";
          port = 993;
        };
        mbsync = oauth2;
        passwordCommand = ''poauth2 refresh /home/jk/.config/aerc/oauth2/ms-work.json "pass show mail/lasis_oauth2" "cat /tmp/lasis" "tee /tmp/lasis"'';
      };
      accounts.sjf = {
        realName = "Joseph Kiem";
        address = "__masked__";
        userName = "__masked__";
        imap = {
          host = "email.zcom.it";
          port = 993;
        };
        mbsync = normal;
        passwordCommand = "pass show sjf/logistics | head -n 1";
      };
      accounts.raika = {
        realName = "Joseph Kiem";
        address = "__masked__";
        userName = "__masked__";
        imap = {
          host = "imap.rolmail.net";
          port = 993;
        };
        mbsync = normal;
        passwordCommand = "pass show mail/raika | head -n 1";
      };
      # accounts.uibk = {
      #   realName = "Joseph Kiem";
      #   address = "__masked__";
      #   userName = "csat1606";
      #   imap = {
      #     host = "mail.uibk.ac.at";
      #     port = 993;
      #   };
      #   mbsync = normal;
      #   passwordCommand = "pass show mail/uibk | head -n 1";
      # };
    };
  };
}
