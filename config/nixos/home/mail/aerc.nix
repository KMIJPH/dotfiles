with import ../common.nix;

let
  sent = {
    index-columns = "date<18,to<30,flags>4,subject<*";
    column-flags = ''{{.Flags | join ""}}'';
    column-subject = "{{.ThreadPrefix}}{{.Subject}}";
    column-date = "{{.DateAutoFormat .Date.Local}}";
    column-to = ''{{.To | persons | join ", "}}'';
  };

  folders-sort = "Inbox, Drafts, Junk, Sent, Trash";

  folder-map-ms = {
    "Junk" = "Junk Email";
    "Deleted Items" = "Deleted";
    "Trash" = "Deleted Items";
    "Sent" = "Sent Items";
  };

  folder-map-sjf = folder-map-ms // {
    "Junk" = "Junk E-Mail";
  };

  convertFolderMap =
    folderMap:
    builtins.concatStringsSep "\n" (
      map (key: "${key} = ${folderMap.${key}}") (builtins.attrNames folderMap)
    );

in
{
  home-manager.users.${user} = {
    programs.aerc = {
      enable = true;
      extraConfig = {
        general = {
          pgp-provider = "internal";
          unsafe-accounts-conf = true; # needed when using home-manager
        };
        ui = {
          auto-mark-read = true;
          border-char-horizontal = "─";
          border-char-vertical = "│";
          dirlist-delay = "200ms";
          dirlist-left = "{{.Folder}}";
          dirlist-right = "{{if .Unread}}{{humanReadable .Unread}}/{{end}}{{if .Exists}}{{humanReadable .Exists}}{{end}}";
          dirlist-tree = false;
          empty-dirlist = "(no folders)";
          empty-message = "(no messages)";
          index-columns = "date<16,name<30,flags>3,subject<*";
          column-date = "{{.DateAutoFormat .Date.Local}}";
          column-name = "{{index (.From | names) 0}}";
          column-flags = ''{{.Flags | join ""}}'';
          column-subject = "{{.ThreadPrefix}}{{.Subject}}";
          column-separator = "│";
          mouse-enabled = false;
          new-message-bell = true;
          next-message-on-delete = true;
          pinned-tab-marker = "'`'";
          sidebar-width = 14;
          styleset-name = "default";
          stylesets-dirs = "~/.config/aerc/stylesets";
          this-day-time-format = "15:04";
          this-week-time-format = "Mon 02 Jan 15:04";
          this-year-time-format = "Mon 02 Jan";
          timestamp-format = "02 Jan 2006";
          threading-enabled = true;
          force-client-threads = true;
        };
        "ui:folder=Sent" = sent;
        "ui:folder=Sent Items" = sent;
        statusline = {
          status-columns = "left<*,right>*";
          column-right = "{{.TrayInfo}}";
          column-left = "[{{.Account}}] {{.StatusInfo}}";
          separator = "|";
          display-mode = "icon";
        };
        viewer = {
          alternatives = "text/plain,text/html";
          always-show-mime = false;
          completion-delay = "250ms";
          completion-popovers = true;
          header-layout = "From|To,Cc|Bcc,Date,Subject";
          pager = "bat --style='grid' --wrap='character' --pager='less -FRXI'";
          show-headers = false;
        };
        compose = {
          editor = apps.editor;
          header-layout = "From,To,Cc,Bcc,Subject";
          address-book-cmd = "sh -c 'cat ~/.config/nixos/home/mail/aerc/list | grep \"%s\"'";
          reply-to-self = false;
          no-attachment-warning = "^[^>]*attach(ed|ment)|an(bei|hang)|allegato";
        };
        filters = {
          "text/plain" = "awk -f ~/.config/nixos/home/mail/aerc/colorize";
          "text/html" = "w3m -I UTF-8 -T text/html | awk -f ~/.config/nixos/home/mail/aerc/colorize";
        };
        templates = {
          template-dirs = "~/.config/aerc/templates";
          new-message = "new_message";
          quoted-reply = "quoted_reply";
          forwards = "forward_as_body";
        };
      };
      extraAccounts = {
        "-Outlook-" = {
          primary = true;
          source = "maildir://~/Maildir/outlook";
          outgoing = "smtp+xoauth2://__masked__@outlook.office365.com:587";
          outgoing-cred-cmd = ''poauth2 refresh /home/jk/.config/aerc/oauth2/ms-private.json "pass show mail/outlook_oauth2" "cat /tmp/ms" "tee /tmp/ms"'';
          default = "Inbox";
          from = "Joseph Kiem <__masked__>";
          check-mail-cmd = "mbsync outlook";
          folder-map = "/home/${user}/.config/aerc/folders/ms";
          folders-sort = folders-sort;
        };
        Lasis = {
          source = "maildir://~/Maildir/lasis";
          outgoing = "smtp+xoauth2://__masked__@outlook.office365.com:587";
          outgoing-cred-cmd = ''poauth2 refresh /home/jk/.config/aerc/oauth2/ms-private.json "pass show mail/outlook_oauth2" "cat /tmp/ms" "tee /tmp/ms"'';
          default = "Inbox";
          from = "Joseph Kiem <__masked__>";
          check-mail-cmd = "mbsync lasis";
          folder-map = "/home/${user}/.config/aerc/folders/ms";
          folders-sort = folders-sort;
        };
        SJF = {
          source = "maildir://~/Maildir/sjf";
          outgoing = "smtps+login://__masked__@email.zcom.it:465";
          outgoing-cred-cmd = "pass show sjf/logistics | head -n 1";
          default = "Inbox";
          from = "Joseph <__masked__>";
          copy-to = "Sent Items";
          check-mail-cmd = "mbsync sjf";
          folder-map = "/home/${user}/.config/aerc/folders/sjf";
          folders-sort = folders-sort;
        };
        Raika = {
          source = "maildir://~/Maildir/raika";
          outgoing = "smtp+login://__masked__@smtp.rolmail.net:587";
          outgoing-cred-cmd = "pass show mail/raika | head -n 1";
          default = "Inbox";
          from = "Joseph Kiem <__masked__>";
          copy-to = "Sent";
          check-mail-cmd = "mbsync raika";
          folders-sort = folders-sort;
        };
        # UIBK = {
        #   source = "maildir://~/Maildir/uibk";
        #   outgoing = "smtp+login://__masked__:587";
        #   outgoing-cred-cmd = "pass show mail/uibk | head -n 1";
        #   default = "Inbox";
        #   from = "Joseph Kiem <Joseph.K__masked__>";
        #   copy-to = "Sent";
        #   check-mail-cmd = "mbsync uibk";
        # };
      };
      extraBinds = {
        global = {
          "<Tab>" = ":next-tab<Enter>";
          "<Backtab>" = ":prev-tab<Enter>";
        };
        messages = {
          q = ":quit<Enter>";
          j = ":next<Enter>";
          k = ":prev<Enter>";
          "<Down>" = ":next<Enter>";
          "<Up>" = ":prev<Enter>";
          "<C-d>" = ":next 50%<Enter>";
          "<C-f>" = ":next 100%<Enter>";
          "<PgDn>" = ":next 100%<Enter>";
          "<C-a>" = ":mark -a -t<Enter>";
          "<C-u>" = ":prev 50%<Enter>";
          "<C-b>" = ":prev 100%<Enter>";
          "<PgUp>" = ":prev 100%<Enter>";
          g = ":select 0<Enter>";
          G = ":select -1<Enter>";
          J = ":next-folder<Enter>";
          K = ":prev-folder<Enter>";
          H = ":collapse-folder<Enter>";
          L = ":expand-folder<Enter>";
          "<Space>" = ":mark -t<Enter>:next<Enter>";
          T = ":toggle-threads<Enter>";
          "<Enter>" = ":view<Enter>";
          d = ":prompt 'Hit enter to delete!' mv Trash<Enter>";
          m = ":compose<Enter>";
          r = ":reply -qa<Enter>";
          f = ":forward -A <Enter>";
          c = ":cf<space>";
          "$" = ":term<space>";
          "|" = ":pipe<space>";
          "/" = ":search -bf<space>";
          "\\" = ":filter -bf<space>";
          n = ":next-result<Enter>";
          p = ":prev-result<Enter>";
          "<Esc>" = ":clear<Enter>";
          "!" = ":flag -t -x Flagged<Enter>";
          "u" = ":flag -t -x Seen<Enter>";
        };
        "messages:folder=Drafts" = {
          "<Enter>" = ":recall<Enter>";
          l = ":recall<Enter>";
        };
        view = {
          "/" = ":toggle-key-passthrough<Enter>/";
          q = ":close<Enter>";
          O = ":open<Enter>";
          S = ":save<space>";
          "|" = ":pipe<space>";
          D = ":delete<Enter>";
          A = ":archive flat<Enter>";
          f = ":forward<Enter>";
          r = ":reply -q<Enter>";
          H = ":toggle-headers<Enter>";
          "<C-k>" = ":prev-part<Enter>";
          "<C-j>" = ":next-part<Enter>";
          J = ":next<Enter>";
          K = ":prev<Enter>";
        };
        "view::passthrough" = {
          "$noinherit" = true;
          "$ex" = "<C-x>";
          "<Esc>" = ":toggle-key-passthrough<Enter>";
        };
        compose = {
          "$ex" = "<C-x>";
          "<C-k>" = ":prev-field<Enter>";
          "<C-j>" = ":next-field<Enter>";
          "<tab>" = ":next-field<Enter>";
        };
        "compose::editor" = {
          "$noinherit" = true;
          "$ex" = "<C-x>";
          "<C-k>" = ":prev-field<Enter>";
          "<C-j>" = ":next-field<Enter>";
          "<C-p>" = ":prev-tab<Enter>";
          "<C-n>" = ":next-tab<Enter>";
        };
        "compose::review" = {
          y = ":send<Enter>";
          n = ":abort<Enter>";
          p = ":postpone<Enter>";
          q = ":choose -o d discard abort -o p postpone postpone<Enter>";
          e = ":edit<Enter>";
          a = ":attach<space>";
          d = ":detach<space>";
        };
        terminal = {
          "$noinherit" = true;
          "$ex" = "<C-x>";
          "<C-p>" = ":prev-tab<Enter>";
          "<C-n>" = ":next-tab<Enter>";
        };
      };
      stylesets = {
        default = ''
                    # __masked__
          *.default=true
          default.bg=#1f1f28
          default.fg=#bcc5d6

          title.reverse=true
          header.bold=true
          header.fg=#dcd7ba

          *error.bold=true
          error.fg=#ff5d62
          warning.fg=#ffa066
          success.fg=#98bb6c

          statusline*.default=true
          statusline_error.fg=#ff5d62
          statusline_default.fg=#363646
          statusline_default.bg=#dcd7ba

          dirlist_recent.selected.fg=#363646
          dirlist_unread.fg=#7fb4ca
          dirlist_unread.selected.fg=#7fb4ca
          dirlist_default.selected.bg=#363646
          dirlist_default.selected.fg=#dcd7ba

          msglist_default.selected.fg=#363646
          msglist_default.selected.bg=#d27e99
          msglist_unread.bold=true
          msglist_unread.fg=#7fb4ca
          msglist_unread.selected.bg=#363646
          msglist_read.selected.fg=#dcd7ba
          msglist_read.selected.bg=#363646
          msglist_marked.fg=#d27e99
          msglist_marked.selected.fg=#dcd7ba
          msglist_marked.selected.bg=#363646
          msglist_deleted.fg=#ff5d62
          msglist_result.fg=#dcd7ba
          msglist_result.selected.bg=#363646

          msglist_deleted.selected.reverse=toggle

          completion_pill.reverse=true

          tab.reverse=true
          border.reverse=true
          tab.bg=#dcd7ba
          tab.fg=#363646
          tab.selected.bg=#363646
          tab.selected.fg=#dcd7ba
          border.fg=#363646

          selector_focused.reverse=true
          selector_chooser.bold=true
        '';
      };
      templates = {
        new_message = "X-Mailer: aerc {{version}}";
        forward_as_body = ''
          X-Mailer: aerc {{version}}

          ----- Forwarded Message -----
          From: {{.OriginalFrom | emails | join ", "}}
          On: {{dateFormat (.OriginalDate | toLocal) "Mon Jan 2, 2006 at 15:04 MST"}}
          Subject: {{.SubjectBase}}

          {{if eq .OriginalMIMEType "text/html"}}{{exec `w3m -I UTF-8 -T text/html` .OriginalText}}{{else}}{{.OriginalText}}{{end}}
        '';
        quoted_reply = ''
          X-Mailer: aerc {{version}}

          ----- Original Message -----
          From: {{.OriginalFrom | emails | join ", "}}
          On: {{dateFormat (.OriginalDate | toLocal) "Mon Jan 2, 2006 at 15:04 MST"}}
          Subject: {{.SubjectBase}}

          {{if eq .OriginalMIMEType "text/html"}}{{exec `w3m -I UTF-8 -T text/html` .OriginalText}}{{else}}{{.OriginalText}}{{end}}
        '';
      };
    };

    xdg.configFile."aerc/folders/ms".text = convertFolderMap folder-map-ms;
    xdg.configFile."aerc/folders/sjf".text = convertFolderMap folder-map-sjf;

    xdg.desktopEntries = {
      # opens aerc in a terminal and adds the mailto:
      aerc = {
        name = "Aerc";
        genericName = "Mail Client";
        comment = "Launches the aerc email client";
        exec = "${apps.terminal} -e aerc %U";
        terminal = false;
        icon = "aerc";
        categories = [
          "Office"
          "Network"
          "Email"
        ];
        mimeType = [ "x-scheme-handler/mailto" ];
      };
    };
  };
}
