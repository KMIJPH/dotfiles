{
  user = "jk";

  version = "24.11";

  fonts = {
    mono = {
      name = "SauceCodePro NFM";
      name_alt = "SourceCodePro";
      package = "nerdfonts";
    };
    serif = {
      name = "Liberation";
      package = "liberation_ttf";
    };
  };

  apps = {
    pdf = "zathura";
    images = "feh";
    writer = "writer";
    powerpoint = "impress";
    spreadsheet = "calc";
    mailer = "aerc";
    filebrowser_gui = "pcmanfm";
    filebrowser_tui = "lf";
    terminal = "kitty";
    editor = "hx";
    browser = "firefox";
    player = "mpv";
  };

  colors = {
    black = "#000000";
    brown = "#875a4c";
    bg = "#1c1c1c";
    grey0 = "#2e3440";
    grey1 = "#3b4252";
    grey2 = "#434c5e";
    grey3 = "#4c566a";
    fg = "#bcc5d6";
    red = "#bf616a";
    orange = "#d08770";
    yellow = "#ebcb8b";
    green0 = "#a3be8c";
    green1 = "#8fbcbb";
    green2 = "#94ff9f";
    purple = "#b48ead";
    blue0 = "#5e81ac";
    blue1 = "#81a1c1";
    blue2 = "#88c0d0";
    blue3 = "#345885";
    white1 = "#e5e9f0";
    white2 = "#eceff4";
  };
}
