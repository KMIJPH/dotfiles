with import ../common.nix;

{
  home-manager.users.${user} = {
    programs.bottom = {
      enable = true;
      settings = {
        processes = {
          columns = [ "PID" "Name" "CPU%" "Mem%" "R/s" "W/s" "User" ];
        };
        row = [
          {
            ratio = 40;
            child = [
              { type = "net"; }
              {
                type = "proc";
                default = true;
              }
            ];
          }
          {
            ratio = 30;
            child = [{ type = "cpu"; }];
          }
          {
            ratio = 30;
            child = [
              {
                ratio = 4;
                type = "mem";
              }
              {
                ratio = 3;
                child = [ { type = "temp"; } { type = "disk"; } ];
              }
            ];
          }
        ];
      };
    };
  };
}
