with import ../common.nix;

{
  home-manager.users.${user} = {
    programs.bash = {
      enable = true;
      historyIgnore = [
        "ls"
        "cd"
        "exit"
      ];
      shellOptions = [
        "histappend"
        "extglob"
        "globstar"
        "checkjobs"
        "cdspell"
        "complete_fullquote"
        "dirspell"
      ];
      shellAliases = {
        ls = "ls -la --group-directories-first --color";
        size = "du -sh";
        qalc = "qalc --set 'angle unit 2' --set 'decimal comma off'";
        sync-mail = "mbsync -a";
        we = "wenn tui ~/.config/nixos/home/desktop/remind/main.rem";
        wex = "wenn export ~/.config/nixos/home/desktop/remind/main.rem > ~/.config/nixos/home/desktop/remind/out.ics";
        mirror = "wget -mpEk";
        panhoc = "pandoc --lua-filter ~/Sync/scripts/html/wiki/static/pandoc-goat-filter.lua --lua-filter ~/Sync/scripts/html/wiki/static/pandoc-image-filter.lua --lua-filter /home/jk/Sync/scripts/html/wiki/static/pandoc-hugo-shortcodes.lua";
      };
      sessionVariables = {
        TERMINAL = apps.terminal;
        EDITOR = apps.editor;
      };
      profileExtra = ''
        if [[ -z $DISPLAY ]] && [[ "$(tty)" = "/dev/tty1" ]]; then
            Hyprland
        elif [[ -z $DISPLAY ]] && [[ "$(tty)" = "/dev/tty2" ]]; then
            exec startx
        fi
      '';
      bashrcExtra = ''
        source ~/.config/nixos/home/desktop/bash-scripts/bibman_bash_completion.sh
        export PS1="\[\e[0;90m\]\D{%H:%M:%S} \[\e[1;34m\](\W)\[\e[0m\] "

        sysinfo() {
            local os mem gpu net boa boa_v
            os=$(grep 'PRETTY_NAME' /etc/os-release | sed -n -e 's/^.*NAME=//p')
            mem=$(lsmem | awk '/Online/{printf("%sB"), $2}')
            gpu=$(lspci | grep VGA | sed -n 's/^.*: //p')
            net=$(lspci | grep net | sed -n 's/^.*: //p')
            boa=$(cat /sys/class/dmi/id/product_name)
            boa_v=$(cat /sys/class/dmi/id/product_version)

            echo -e "-------------------------------System Information----------------------------
        Hostname:\t\t$HOSTNAME
        Active User:\t\t$(w | cut -d ' ' -f1 | grep -v USER | xargs -n1)
        Uptime:\t\t\t$(uptime | awk '{print $2,$3}' | sed 's/,//')

        Manufacturer:\t\t$(cat /sys/class/dmi/id/chassis_vendor)
        Product Name:\t\t$boa $boa_v
        Architecture:\t\t$(uname -m)

        CPU:\t\t\t$(awk -F':' '/^model name/ {print $2}' /proc/cpuinfo | uniq | sed -e 's/^[ \t]*//')
        GPU:\t\t\t$gpu
        Network Controller:\t$net
        Total Memory:\t\t$mem

        Operating System:\t$(uname -o) $os
        Kernel:\t\t\t$(uname -r)
        -----------------------------------------------------------------------------"
        }
        dl() {
            local url="$1"
            local template="./%(artist)s_%(album)s_%(track)s_%(title)s.%(ext)s"
            local cmd="yt-dlp -x --audio-format mp3 --audio-quality 0 -o '$template' '$url'"

            eval "$cmd"
        }
        showip() {
            local ip
            ip=$(ip addr | grep "inet .* scope global" | awk '{print $2}' | sed 's|/.*||g')
            echo "$ip"
        }
        restart_touchpad() {
            # lsmod | grep touch
            sudo rmmod hid_multitouch
            sudo modprobe hid_multitouch
        }
      '';
    };
  };
}
