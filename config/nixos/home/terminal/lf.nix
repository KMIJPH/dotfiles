{ pkgs, ... }:
with import ../common.nix;

{
  home-manager.users.${user} = {
    programs.lf = {
      enable = true;
      settings = {
        drawbox = true;
        dirfirst = true;
        icons = true;
        ifs = "\\n";
        ignorecase = true;
        info = "time:size";
        preview = true;
        ratios = "1:2:2";
        scrolloff = 10;
        shell = "bash";
        shellopts = "-euf";
        incsearch = true;
      };
      commands = {
        on_cd = "%printf '\\033]0;lf: $PWD\\007' > /dev/tty";
        bulk_rename = "$lf-actions bulk-rename";
      };
      keybindings = {
        R = "reload";
        "-" = "jump-prev";
        "\\+" = "jump-next";
        f = "filter";
        c = ":clear; unselect; setfilter";
        "<esc>" = ":clear; unselect; setfilter";
        "<c-a>" = ":unselect; invert";
        "\\#" = ":\${{clear}}; $$SHELL";
        "<backspace>" = "set hidden!";
        g = "";
        ga = "cd ~/pCloudDrive/Archive";
        gb = "cd ~/.config/nixos/home/desktop/bib";
        go = "cd ~/pCloudDrive/Docs/UIBK/2024_SS";
        gs = "cd ~/Sync/scripts";
        gl = "cd ~/.local";
        gd = "cd /mnt/Data";
        gm = "cd /mnt/Music";
        gt = "cd /tmp";
        gc = "cd ~/.config";
        gu = "cd /run/media/jk";
        s = "";
        sa = ":{{set sortby atime; set info 'atime:size'; }}";
        sc = ":{{set sortby ctime; set info 'ctime:size'; }}";
        se = ":{{set sortby ext; set info 'time:size'; }}";
        sn = ":{{set sortby natural; set info 'time:size'; }}";
        ss = ":{{set sortby size; set info 'time:size'; }}";
        st = ":{{set sortby time; set info 'time:size'; }}";
        "<enter>" = "%{{lf-actions file-open}}";
        "<right>" = "%{{lf-actions file-open}}";
        a = "%{{lf-actions file-create}}";
        o = "%{{lf-actions file-open-with one}}";
        O = "%{{lf-actions file-open-with all}}";
        x = "%{{lf-actions file-execute}}";
        X = "%{{lf-actions file-toggle-chmod}}";
        d = "";
        dD = "\${{clear; lf-actions file-trash}}";
        dF = "\${{clear; lf-actions file-remove}}";
        dd = "%{{lf-actions file-cut}}";
        y = "";
        yp = "\${{lf-actions file-info path &}}";
        yn = "\${{lf-actions file-info name &}}";
        yd = "\${{lf-actions file-info dir &}}";
        yy = "%{{lf-actions file-copy}}";
        p = "";
        pp = "%{{lf-actions file-paste normal}}";
        po = "%{{lf-actions file-paste overwrite}}";
        pb = "%{{lf-actions file-paste backup}}";
        "\\$" = "%{{lf-actions file-run-cmd}}";
        "\\%" = "push :glob-select<space>";
        e = "\${{lf-actions open-editor}}";
        m = "";
        mk = "%{{lf-actions mkdir}}";
        t = "";
        tc = "\${{clear; lf-actions trash-clear}}";
        tr = "\${{clear; lf-actions trash-restore}}";
        E = "%{{lf-actions archive-extract}}";
        Z = "%{{lf-actions archive-create}}";
      };
      extraConfig = ''
        on_cd
        ''${{lf-actions clear-tmp}}
        set infotimefmtnew "Jan _2 15:04"
        set infotimefmtold "Jan _2 2006"
        set cleaner "~/.config/lf/cleaner"
      '';
      previewer.source = pkgs.writeShellScript "previewer.sh" ''
        FILE="$1"

        [ -L "''${FILE}" ] && FILE="$(readlink "''${FILE}")"

        case "$(file -b --mime-type "''${FILE}")" in
            image/*)
                kitten icat --transfer-mode file --stdin no --place "$2x$3@$4x$5" "$FILE" < /dev/null > /dev/tty
                exit 1
                ;;
            text/* | */json)
                bat --theme=Nord --color=always --style=plain "''${FILE}"
                exit 0
                ;;
            audio/* | video/*)
                mediainfo "''${FILE}"
                exit 0
                ;;
            */zip)
                echo "archive"
                exit 0
                ;;
            application/pdf)
                index=$(mktemp /tmp/lf-pdf_preview-index.XXXXXXXXXX)
                pdftotext -f 1 -l 3 "''${FILE}" "$index"
                bat --theme=Nord --color=always --style=plain "''${index}"
                rm "$index"
                exit 0
                ;;
            application/x-hdf | application/x-hdf5)
                netcdf-info "''${FILE}" || \
                echo "netCDF file"
                exit 0
                ;;
            application/*)
                echo "binary"
                exit 0
                ;;
        esac
      '';
    };
    xdg.configFile."lf/icons".source = ./lf/icons;
    xdg.configFile."lf/colors".source = ./lf/colors;
    xdg.configFile."lf/cleaner" = {
      source = ./lf/cleaner;
      executable = true;
    };
  };
}
