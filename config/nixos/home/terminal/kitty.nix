with import ../common.nix;

{
  home-manager.users.${user} = {
    programs.kitty = {
      enable = true;
      keybindings = {
        "ctrl+shift+c" = "copy_to_clipboard";
        "ctrl+shift+v" = "paste_from_clipboard";
        "ctrl+up" = "scroll_line_up";
        "ctrl+down" = "scroll_line_down";
        "ctrl+shift+up" = "scroll_page_up";
        "ctrl+shift+down" = "scroll_page_down";
        "ctrl+plus" = "change_font_size all +1.0";
        "ctrl+minus" = "change_font_size all -1.0";
      };
      extraConfig = ''
        font_family             ${fonts.mono.name} Light
        bold_font               ${fonts.mono.name} Medium
        italic_font             ${fonts.mono.name} Light Italic
        bold_italic_font        ${fonts.mono.name} Medium Italic
        font_size               13.0
        disable_ligatures       never
        cursor_blink_interval   0
        confirm_os_window_close 0
        enable_audio_bell       0
        background_opacity      0.8

        background ${colors.bg}
        foreground ${colors.fg}
        selection_foreground ${colors.bg}
        selection_background ${colors.blue1}
        color0  ${colors.grey1}
        color8  ${colors.grey3}
        color1  ${colors.red}
        color9  ${colors.red}
        color2  ${colors.green0}
        color10 ${colors.green0}
        color3  ${colors.yellow}
        color11 ${colors.yellow}
        color4  ${colors.blue1}
        color12 ${colors.blue1}
        color5  ${colors.purple}
        color13 ${colors.purple}
        color6  ${colors.blue2}
        color14 ${colors.green1}
        color7  ${colors.white1}
        color15 ${colors.white2}
      '';
    };
  };
}
