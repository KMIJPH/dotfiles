with import ../common.nix;

{
  home-manager.users.${user} = {
    programs.lazygit = {
      enable = true;
      settings = {
        timeFormat = "02 Jan 06";
        shortTimeFormat = "15:04";
        nerdFontsVersion = "3";
        border = "rounded";
        keybinding.universal = {
          pushFiles = "p";
          pullFiles = "P";
        };
      };
    };
  };
}
