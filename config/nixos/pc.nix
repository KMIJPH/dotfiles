{ pkgs, ... }:

with import ./home/common.nix;
let
  home-manager = builtins.fetchTarball "__masked__";
  musnix = builtins.fetchTarball "__masked__";
  music_packages = with pkgs; [
    # surge-XT
    # geonkick
    airwindows-lv2
    aether-lv2
    carla
    eq10q
    cardinal
    drumgizmo
    dragonfly-reverb
    ChowKick
    ChowPhaser
    ChowCentaur
    CHOWTapeModel
    delayarchitect
    sfizz
    lsp-plugins
    stochas
    distrho-ports
    zam-plugins
    x42-plugins
    ardour
  ];
in
{
  imports = [
    ./components/boot.nix
    ./components/hardware.nix
    ./components/packages.nix
    ./components/settings.nix
    (import "${home-manager}/nixos")
    ./home
    (import musnix)
  ];

  networking.hostName = "desktop";

  # create directories
  systemd.tmpfiles.rules = [
    "d /mnt/Data 755 jk"
    "d /mnt/Music 755 jk"
  ];

  # music stuff
  musnix = {
    enable = true;
    alsaSeq.enable = true;
    kernel = {
      realtime = false;
      packages = pkgs.linuxPackages_6_4_rt;
    };
    rtirq.enable = false;
    das_watchdog.enable = false;
  };

  # environment.etc = {
  #   "pipewire/pipewire.conf.d/92-low-latency.conf".text = ''
  #     context.properties = {
  #       default.clock.rate = 48000
  #       node.quantum = 256/48000
  #       default.clock.quantum = 256
  #     }
  #   '';
  # };

  # steam
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };

  # virt
  # virtualisation.libvirtd.enable = true;
  # virtualisation.virtualbox.host.enable = true;
  # virtualisation.virtualbox.guest.enable = true;
  # virtualisation.virtualbox.guest.x11 = true;
  # users.extraGroups.vboxusers.members = [ "jk" ];
  # virtualisation.docker.enable = true;

  # extra packages
  environment.systemPackages =
    with pkgs;
    music_packages
    ++ [
      gdal
      darktable
      qgis-ltr
      blender
      prismlauncher
    ];

  system.stateVersion = version;
}
