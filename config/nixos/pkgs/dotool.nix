{ stdenv, pkgs }:

stdenv.mkDerivation {
  pname = "dotool";
  version = "0.0.1";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/dotool.git";
    ref = "main";
    rev = "e5db6b93c29d4a07c9fd1e14b0c584eaded1d6ce";
  };

  nativeBuildInputs = with pkgs; [ go git ];

  buildPhase = ''
    export HOME=$(pwd)
    go build -C src -o ../dotool
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp dotool $out/bin/dotool
  '';
}
