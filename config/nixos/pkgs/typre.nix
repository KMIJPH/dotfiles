{
  pkgs ? import <nixpkgs> { },
}:

pkgs.haskellPackages.mkDerivation {
  pname = "typre";
  version = "0.2.0";
  license = "GPL-3.0-or-later";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/typre.git";
    ref = "main";
    rev = "2cac748bb14e59da7515e66ba928b5598813914c";
  };

  isLibrary = true;
  isExecutable = true;

  executableHaskellDepends = with pkgs.haskellPackages; [
    base
    optparse-applicative
    bytestring
    directory
    filepath
    http-client
    http-client-tls
    http-types
    megaparsec_9_6_1
    process
    hspec
    text
    zlib
  ];
}
