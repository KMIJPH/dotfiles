{ stdenv, pkgs }:

stdenv.mkDerivation {
  pname = "bash-scripts";
  version = "0.0.1";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/bash-scripts.git";
    ref = "main";
    rev = "83746fb3c77f9d4fe40df33c02de0571616c2d6b";
  };

  nativeBuildInputs = with pkgs; [
    file
    jq
    rsync
    # python
    # bibman
    # trash-cli
    # wl-clipboard
    # xsel
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp src/* $out/bin
    chmod +x $out/bin/*

    ln -s ${pkgs.file.out}/bin/file $out/bin/file
    ln -s ${pkgs.jq.bin}/bin/jq $out/bin/jq
    ln -s ${pkgs.rsync.out}/bin/rsync $out/bin/rsync
  '';
}
