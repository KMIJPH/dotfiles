{ pkgs ? import <nixpkgs> { } }:
pkgs.haskellPackages.mkDerivation {
  pname = "mulk";
  version = "0.1";
  license = "GPL-3.0-or-later";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/mulk.git";
    ref = "main";
    rev = "18887e0e304e737b810fb65ad390979be7f39241";
  };

  isLibrary = true;
  isExecutable = true;

  executableHaskellDepends = with pkgs.haskellPackages; [
    base
    HaskellNet
    HaskellNet-SSL
    hspec
    megaparsec_9_6_1
    optparse-applicative
    text
    zlib
  ];
}
