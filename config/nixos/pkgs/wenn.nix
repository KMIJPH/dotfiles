{ pkgs }:

pkgs.python3.pkgs.buildPythonApplication {
  pname = "wenn";
  version = "0.1.2";
  format = "pyproject";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/wenn.git";
    ref = "main";
    rev = "79753cefa7e08331a9f235f1b24980470e103de3";
  };

  nativeBuildInputs = (with pkgs.python3.pkgs; [ setuptools ]);
  propagatedBuildInputs = with pkgs.python3.pkgs; [ textual ];

  postInstall = ''
    cp $src/src/tui/tui.tcss $out/lib/python3.12/site-packages/tui/tui.tcss
  '';
}
