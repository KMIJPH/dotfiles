{ pkgs }:

pkgs.python3.pkgs.buildPythonApplication {
  pname = "poauth2";
  version = "0.1";
  format = "pyproject";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/poauth2.git";
    ref = "main";
    rev = "d31e161fae32d9de6e3816c9dbdf49e431290259";
  };

  nativeBuildInputs = with pkgs.python3.pkgs; [ setuptools ];
}
