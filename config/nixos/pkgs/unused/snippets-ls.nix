{ fetchFromGitHub, buildGoModule }:

buildGoModule rec {
  pname = "snippets-ls";
  version = "0.0.4";

  src = fetchFromGitHub {
    owner = "quantonganh";
    repo = "snippets-ls";
    rev = "v${version}";
    sha256 = "sha256-6N7oUpqpuyxcmTf6cQf6j1O6aRR32lnaIJClK23lAtQ=";
  };

  vendorHash = "sha256-0FGBtSYKaSjaJlxr8mpXyRKG88ThJCSL3Qutf8gkllw=";
}
