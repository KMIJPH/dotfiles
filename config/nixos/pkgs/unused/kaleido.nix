# { stdenv, pkgs }:
#
# stdenv.mkDerivation {
#   pname = "kaleido";
#   version = "0.2.1";
#
#   nativeBuildInputs = with pkgs; [ pip ldd patchelf ];
#
#   installPhase = ''
#     TARGET=./output/kaleido/executable/bin/kaleido
#     # file ./output/kaleido/executable/bin/kaleido
#     # ldd ./output/kaleido/executable/bin/kaleido
#     pip install --target=./output kaleido
#
#     find /nix/store -name 'ld-linux-x86-64.so.2'
#     patchelf --set-interpreter /nix/store/qn3ggz5sf3hkjs2c797xf7nan3amdxmp-glibc-2.38-27/lib/ld-linux-x86-64.so.2 "$TARGET"
#
#     find /nix/store -name 'libnss3.so' -type f -exec dirname {} \;
#     find /nix/store -name 'libnssutil3.so' -type f -exec dirname {} \;
#     find /nix/store -name 'libnspr4.so' -type f -exec dirname {} \;
#     find /nix/store -name 'libexpat*'
#
#     patchelf --set-rpath /nix/store/7bj4kp0yf0camdasb88z4k4gy0dgm0ak-expat-2.5.0/lib/:/nix/store/pw8v6vs8ji8vixhpkpkqnj206865l1sn-nspr-4.35/lib:/nix/store/9wqlaq512844ihrml40mrpd50126fr7x-nss-3.79.4/lib "$TARGET"
#     ldd "$TARGET"
#
#     # change in scopes/base.py:
#     # proc_args = self._build_proc_args()
#     # to:
#     # proc_args = ["sh"] + self._build_proc_args()
#   '';
# }

{ buildPythonPackage, lib, nss, nspr, expat, fetchPypi }:
let rpath = lib.makeLibraryPath [ nss nspr expat ];
in buildPythonPackage rec {
  pname = "kaleido";
  version = "0.2.1";
  format = "wheel";
  src = fetchPypi {
    inherit pname version format;
    platform = "manylinux1_x86_64";
    hash = "sha256-qiHPG/HHj4+lCp99ReEAPDh709b+CnZ8+780S5W9w6g=";
  };
  doCheck = false;
  postFixup = ''
    for file in $(find $out -type f \( -perm /0111 -o -name \*.so\* \) ); do
      patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" "$file" || true
      patchelf --set-rpath ${rpath}:$out/lib/x86_64-linux-gnu $file || true
    done
    sed -i 's,#!/bin/bash,#!/usr/bin/env bash,' $out/lib/python3.11/site-packages/kaleido/executable/kaleido
  '';
}

# let
#   pkgs = import <nixpkgs> {};
#   kaleido = pkgs.python3Packages.callPackage ./kaleido.nix {};
# in pkgs.mkShell {
#   packages = with pkgs; [
#     python3
#     pkgs.python3Packages.plotly
#     pkgs.python3Packages.pandas
#     pkgs.python3Packages.packaging
#     kaleido
#   ];
# }
