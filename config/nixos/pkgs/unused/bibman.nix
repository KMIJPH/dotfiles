{
  pkgs ? import <nixpkgs> { },
}:

pkgs.haskellPackages.mkDerivation {
  pname = "bibman";
  version = "0.4.0";
  license = "GPL-3.0-or-later";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/bibman.git";
    ref = "main";
    rev = "0195692d5e7323b4dabc4b85f1379cd538d8a6bd";
  };

  isLibrary = true;
  isExecutable = true;

  executableHaskellDepends = with pkgs.haskellPackages; [
    aeson
    base
    brick
    bytestring
    containers
    configurator
    filepath
    hspec
    http-client
    http-client-tls
    lens
    microlens
    microlens-mtl
    microlens-th
    network-uri
    pandoc_3_5
    pandoc-types
    parsec
    prettyprinter
    prettyprinter-ansi-terminal
    optparse-applicative
    process
    text
    vector
    vty
    zlib
  ];
}
