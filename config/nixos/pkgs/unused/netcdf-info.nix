{ rustPlatform, pkgs }:

rustPlatform.buildRustPackage {
  pname = "netcdf-info";
  version = "0.1.0";

  src = builtins.fetchGit {
    url = "htt" + "ps://codeberg.org/KMIJPH/netcdf-info.git";
    ref = "main";
    rev = "97b31f1df3111e06adfe505932c902d0c6a0311f";
  };

  cargoHash = "sha256-3hLR5a6Zg1t3pqH5o3S5SsK+wQf7jVoG9pRPro0vupc=";

  nativeBuildInputs = with pkgs; [ cargo rustc cmake ];
}
