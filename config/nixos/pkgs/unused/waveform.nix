{ lib, stdenv, pkgs }:

stdenv.mkDerivation (finalAttrs: {
  pname = "waveform";
  version = "13.0.0";

  src = pkgs.fetchurl {
    url =
      "__masked__";
    hash = "sha256-aG1nhbF1bgc651cmy6Fc1l9H/DjlRPAIr7CV4432Q5M=";
  };

  buildInputs = with pkgs; [
    libGL
    libusb1
    alsaLib
    freetype
    gcc
    webkitgtk
    gtk4
    curl
  ];

  nativeBuildInputs = with pkgs; [ autoPatchelfHook dpkg ];

  unpackPhase = ''
    dpkg -x $src unpacked
  '';

  installPhase = ''
    autoPatchelf unpacked/usr/bin/Waveform13
    mkdir -p $out/bin
    cp unpacked/usr/bin/Waveform13 $out/bin/Waveform13
  '';

  meta = with lib; {
    homepage = "__masked__";
    description = "Tracktion Waveform Free";
    platforms = platforms.unix;
  };
})
