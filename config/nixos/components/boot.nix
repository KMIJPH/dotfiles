{ pkgs, ... }:

{
  # bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # watchdog
  boot.kernel.sysctl = {
    "kernel.nmi_watchdog" = 0;
    "vm.dirty_writeback_centisecs" = 6000;
  };

  # wifi and audio driver power
  boot.extraModprobeConfig = ''
    options iwlwifi power_save=1
    options iwlmvm power_scheme=3
    options snd_hda_intel power_save=1
  '';

  # SATA power management
  services.udev.extraRules = ''
    ACTION=="add",
    SUBSYSTEM=="scsi_host",
    KERNEL=="host*",
    ATTR{link_power_management_policy}="med_power_with_dipm"
  '';

  # processor power management
  systemd.services.powerprofile = {
    wantedBy = [ "multi-user.target" ];
    path = [ pkgs.coreutils ];
    enable = true;
    serviceConfig = {
      User = "root";
      Group = "root";
    };
    script = ''
      echo "w /sys/devices/system/cpu/cpufreq/policy?/energy_performance_preference - - - - balance_power" |
      tee /etc/tmpfiles.d/energy_performance_preference.conf
    '';
  };

  services.thermald.enable = true;
}
