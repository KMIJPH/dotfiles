{ pkgs, ... }:

{
  # nix
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowBroken = false;
  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
  ];
  nix.gc = {
    automatic = true;
    dates = "05:00:00";
    options = "--delete-older-than 7d";
  };

  # users
  users.users.jk = {
    isNormalUser = true;
    description = "jk";
    extraGroups = [
      "networkmanager"
      "wheel"
      "video"
      "seat"
      "audio"
      "input"
      "libvirtd"
      "docker"
    ];
    packages = [ ];
  };

  # locale
  time.timeZone = "Europe/Rome";
  i18n.defaultLocale = "de_DE.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_DE.UTF-8";
    LC_IDENTIFICATION = "de_DE.UTF-8";
    LC_MEASUREMENT = "de_DE.UTF-8";
    LC_MONETARY = "de_DE.UTF-8";
    LC_NAME = "de_DE.UTF-8";
    LC_NUMERIC = "de_DE.UTF-8";
    LC_PAPER = "de_DE.UTF-8";
    LC_TELEPHONE = "de_DE.UTF-8";
    LC_TIME = "de_DE.UTF-8";
  };
  console.keyMap = "de";
  services.xserver = {
    xkb = {
      layout = "de";
      variant = "";
    };
  };

  # env
  # environment.sessionVariables = {
  #   XDG_CURRENT_DESKTOP = "hyprland"; # firefox screenshare
  # };

  environment.localBinInPath = true;

  # xdg
  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [
      xdg-desktop-portal-gtk
      xdg-desktop-portal-wlr # firefox screenshare
    ];
  };

  # swaylock fix
  security.pam.services.swaylock = { };

  # fonts
  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      liberation_ttf
      (nerdfonts.override { fonts = [ "SourceCodePro" ]; })
    ];
    fontconfig = {
      defaultFonts = {
        serif = [ "Liberation" ];
        sansSerif = [ "Liberation" ];
        monospace = [ "SauceCodePro NFM" ];
      };
    };
  };
}
