{
  # network
  networking.firewall.enable = true;
  networking.enableIPv6 = false;
  networking.networkmanager.enable = true;

  # bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # nfs
  networking.hosts = { "192.168.1.124" = [ "NAS" ]; };
  services.nfs.server = { enable = true; };
  fileSystems."/mnt/NAS" = {
    device = "NAS:/";
    fsType = "nfs";
    options = [ "x-systemd.automount" "noauto" ];
  };

  # sound
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  # video
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
  };
}
