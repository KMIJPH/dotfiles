{ pkgs, ... }:

let
  lang-purs = with pkgs; [
    purs-tidy
    purescript-language-server
    nodejs
    esbuild
    purs-unstable
    spago-unstable
  ];
  lang-hs = with pkgs; [
    ghc
    zlib
    pkg-config
    haskell-language-server
    haskellPackages.cabal-fmt
    haskellPackages.cabal-install
    haskellPackages.ormolu
    haskellPackages.hakyll
    haskellPackages.zlib
  ];
  lang-bash = with pkgs; [
    nodePackages_latest.bash-language-server
    shellcheck
    shfmt
  ];
  lang-python = with pkgs; [
    (python311.withPackages (
      ps: with ps; [
        ipython
        pip
        autopep8
        pylsp-mypy
        pylsp-rope
        python-lsp-ruff
        python-lsp-server
        pyyaml
      ]
    ))
  ];
  lang-nix = with pkgs; [
    nil
    nixfmt-rfc-style
  ];
  lang-rust = with pkgs; [
    cargo
    rust-analyzer
    rustc
    rustfmt
  ];
  lang-go = with pkgs; [
    go
    gopls
  ];
  lang-lua = with pkgs; [
    lua
    lua-language-server
    luarocks-nix
    lua52Packages.tl
  ];
  lang-typst = with pkgs; [
    typst
    typstyle
    tinymist
  ];
  lang-typescript = with pkgs; [
    typescript
    typescript-language-server
    nodePackages_latest.prettier
    nodejs
  ];
  lang-julia = with pkgs; [ julia ];
  lang-zig = with pkgs; [
    zig
    zls
  ];
  purescript-overlay = builtins.getFlake "github:thomashoneyman/purescript-overlay";
in
{
  nixpkgs.overlays = [
    (import ../overlays/tex.nix)
    (import ../overlays/cyrus-sasl-xoauth2.nix)
    (import ../overlays/pcloud.nix)
    purescript-overlay.overlays.default
  ];

  environment.systemPackages =
    with pkgs;
    lang-purs
    ++ lang-hs
    ++ lang-bash
    ++ lang-nix
    ++ lang-rust
    ++ lang-go
    ++ lang-lua
    ++ lang-python
    ++ lang-typst
    ++ lang-julia
    ++ lang-typescript
    ++ lang-zig
    ++ [
      (callPackage ../pkgs/bash-scripts.nix { })
      (callPackage ../pkgs/dotool.nix { })
      (callPackage ../pkgs/wenn.nix { })
      (callPackage ../pkgs/pdf-tools.nix { })
      (callPackage ../pkgs/poauth2.nix { })
      (callPackage ../pkgs/typre.nix { })
      (callPackage ../pkgs/mulk.nix { })
      # (callPackage ../pkgs/bibman.nix { })
      appimage-run
      bat
      bemenu
      bottom
      calibre
      cyrus_sasl # aerc
      element-desktop
      fd
      firefox
      gcc
      gimp
      gnupg
      grim # screenshot
      helix
      heroic
      hyprsunset
      hyprpaper
      hyprlock
      feh
      just
      jre8
      less
      libappindicator-gtk3 # tray icons
      libqalculate
      libreoffice-fresh
      librsvg # for pandoc
      mediainfo
      networkmanagerapplet
      nodePackages_latest.npm
      openvpn
      pandoc
      pass
      pavucontrol
      pciutils
      pcmanfm
      pdfpc
      pinentry
      pinentry-gtk2
      pcloud-fixed
      playerctl
      poppler_utils
      pulseaudio
      remind
      ripgrep
      slurp # screenshot
      tex-pkgs
      teams-for-linux
      trash-cli
      tree-sitter
      udiskie
      unzip
      virtualenv
      w3m
      wget
      wl-clipboard
      wl-mirror
      xdg-utils # xdg-open
      xsel
      xwayland
      yt-dlp
      zip
    ];

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
    extraCompatPackages = [ pkgs.proton-ge-bin ];
  };

  programs.hyprland.enable = true;
  security.pam.services.hyprlock = { };
  programs.nix-ld.enable = true;
  services.udisks2.enable = true;

}
