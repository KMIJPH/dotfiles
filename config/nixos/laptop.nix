{ pkgs, ... }:

with import ./home/common.nix;
let
  home-manager = builtins.fetchTarball
    "__masked__";

in {
  imports = [
    ./components/boot.nix
    ./components/hardware.nix
    ./components/packages.nix
    ./components/settings.nix
    (import "${home-manager}/nixos")
    ./home
  ];

  environment.systemPackages = with pkgs; [ mixxx gromit-mpx ];

  hardware.graphics.extraPackages = with pkgs; [ intel-media-driver vaapiIntel ];
  environment.sessionVariables.LIBVA_DRIVER_NAME = "iHD";

  networking.hostName = "laptop";
  system.stateVersion = version;
}
